# Gefahrengutunfall - *Erkennen und Vorsichtsmaßnahmen*

Zur Identifikation eines gefährlichen Stoffes gibt es folgende Möglichkeiten:

+ Kennzeichnung
  + Gefahrzettel
  + Gefahrtafel
    + Gefahrnummer
    + UN-Nummer
  + Am Wasser: [blaue Kegel](https://de.wikipedia.org/wiki/Kegelschiff)
    + 1: entzündlich
    + 2: gesundheitsschädlich
    + 3: explosiv
+ Beförderungspapier
+ Sicherheitsdatenblatt

Zum Abschätzen der ausgetretenen Menge ist es hilfreich zu wissen, aus welchem Behälter der Stoff ausgetreten ist:

| Behälter | Menge |
| --- | --- |
| Flasche | 0,1 - 5L |
| Kanister | 5 - 20L |
| Fass | 20 - 200L |
| [Intermediate Bulk Container (IBC)](https://de.wikipedia.org/wiki/Intermediate_Bulk_Container) | 500 - 3000L |
| Tank-LKW, Kesselwagen | ~10.000L |
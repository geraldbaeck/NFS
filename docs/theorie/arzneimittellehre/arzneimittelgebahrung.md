title:          Arzneimittelgebarung
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Arzneimittellehre
                Notfallmedizin
md5:            eb79876b9436bc5f04ba3099fcda09e9
date_modified:  2019/04/04
date:           2019/04/04

## Lagerung im Fahrzeug/Ampullatorium

+ alphabetisch nach Wirkstoff
+ alt vor neu
+ Temperatur kühl
+ Ablaufdatum nur gültig bei **sachgerechter Lagerung**:
    + dunkel
    + erschütterungsfrei
    + keine Temperaturschwankungen
+ Sondermüll wenn:
    + ausgeflockt
    + trüb
    + gefroren

## Umgang mit Suchtgiften

Der Umgang mit Suchtgiften ist nur für NotärztInnen freigegeben; Sie sind auf der Station und im Fahrzeug versperrt – Ausgabe und Verwaltung über ein „Suchtgiftbuch“ das beim IS in einem Safe deponiert ist.
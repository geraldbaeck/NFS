title:          Combivent
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Arzneimittellehre
                Notfallmedizin
md5:            319c3222fadf32678aaa0d16bdaea016
date_modified:  2019/06/18
date:           2019/04/04

| | |
| --- | --- |
| Wirkstoff | Ipratropiumbromid, Salbutamolsulfat |
| Wirkung | β2-Agonismus in der Lunge, Bronchodilatation |
| Wirkdauer | innerhalb weniger Minuten, 4-5h anhaltend |
| Indikation | Akuter Bronchospasmus |
| Klinik | Dyspnoe, Husten, zäher Schleim, Einsetzen der Atemhilfsmuskulatur, exspiratorischer Stridor, Angst |
| Dosierung | 2,5ml Verneblermaske, nicht wiederholbar, mindestens 8l O&#8322; |
| Nebenwirkungen | Herzstolpern, Schwindel, Kopfschmerzen, Tremor |

## Patienteninformation

+ Aufklärung über Maßnahme
+ Herzklopfen möglich
+ Inhalationstechnik

## Kontraindikationen

+ HF > 140
+ Alter < 8 Jahre
+ Schwangerschaft, Stillperiode
+ MCI in den letzten 3 Monaten
+ Bekannte Aortenstenose
+ Bereits erfolgte Gabe
+ Unverträglichkeit
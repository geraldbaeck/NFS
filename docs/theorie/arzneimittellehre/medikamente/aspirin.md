title:          Aspirin ASS
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Arzneimittellehre
                Notfallmedizin
md5:            9b5d937431537bf90ee789948860f8c1
date_modified:  2019/06/13
date:           2019/04/04

| | |
| --- | --- |
| Wirkstoff | Acetylsalicylsäure |
| Wirkmechanismus | Prostaglandin-H2-Synthase-Hemmer => Thrombozyten können Enzyme nachbilden => irreversible gerinnungshemmende Wirkung => Wirkungsdauer = Überlebenszeit der Thrombozyten (8–11 Tage) |
| Wirkung | schmerzstillend, fiebersenkend, gerinnungshemmend |
| Indikation | ACS |
| Klinik | Thoraxschmerz, evtl. Ausstrahlung, Übelkeit, Kaltschweißigkeit, Angst. Cave: Frauen; DM! |
| Dosierung | 1/2 Aspirin Kautablette 500mg = 250mg |
| Nebenwirkungen | Kopfschmerzen, Schwindel, Magen-Darm Beschwerden, GI-Blutungen. Cave: Ulcus ventriculi. |

## PatientInneninformation

+ Aufklärung über Maßnahmen
+ Verabreichung als Kautablette oder Tablette zum Schlucken

## Kontraindikationen

+ Bekannte Unverträglichkeit gegen Acetylsalicylsäure
+ Verdacht auf Aortendissektion / -ruptur
+ Akute gastrointestinale Blutung
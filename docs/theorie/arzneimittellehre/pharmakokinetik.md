title:          Pharmakokinetik
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Arzneimittellehre
                Notfallmedizin
md5:            277e179515850264ce6c39b08e897ee0
date_modified:  2019/04/04
date:           2019/04/04

## Definition

Was macht der Körper mit dem Medikament? Beschreibt die Gesamtheit aller Prozesse, denen ein Arzneistoff im Körper unterliegt.

LADME:

+ Freisetzung (**L**iberation)
+ Absorption / Resorption (**A**ufnahme)  
+ **D**istribution  (Verteilung)
+ **M**etabolisierung (Verstoffwechslung)  
+ **E**xkretion (Ausscheidung)

## Liberation

+ Freisetzung des Wirkstoffes aus der Arzneiform am Applikationsort
+ Ziel ist die Überführung in eine resorptionsfähige und gelöste Form
+ Faktoren:
    + Zerfall der Arzneiform
    + Auflösung des Wirkstoffes
    + Auflösung des Trägermaterials
    + Diffusion des Arzneistoffs durch den Überzug

Hat die Freisetzung des Wirkstoffes schon außerhalb des Körpers stattgefunden, spricht man vom ADME-Prinzip. Beispielsweise ist der Wirkstoff bei Flüssigkeiten (Säfte, Tropfen) schon in Lösung. Auch bei Brausetabletten liegt der Wirkstoff vor einer Applikation gelöst vor. Aus diesem Grund findet man in der Literatur oft die folgende Schreibweise: (L)ADME.

### Applikationsformen

| Abk. | auf gscheit | Erklärung |
| --- | --- | --- |
| i.v. | intravenös | direkt in die Vene |
| subl./s.l. | sublingual | unter die Zunge |
| i.m. | intramuskulär | in den Muskel |
| i.o. | intraossär | in den Knochen  |
| s.c. | subkutan | unter die Haut (Subkutis) |
| p.o. | oral / peroral | über den Mund |
|  | rektal | über den Mastdarm |
|  | transdermal | Haut (Salben, Pflaster,..) |
|  | enteral | Zufuhr über den Magen-Darm Trakt zb oral |
|  | systemisch | Applikationsform, die zu einer Wirkstoffverteilung im gesamten Körper führt |
|  | topisch | örtlich begrenzte Anwendung mit dem Ziel einer örtlich begrenzten Wirkung |

## Absorption

= Aufnahme des Stoffes durch eine biologische Membran in die Blutbahn

## Distribution

= Umverteilung des Wirkstoffs zwischen dem Blutkreislauf und anderen Körperkompartimenten, wie Geweben oder Organen

### First-Pass-Effekt

+ Medikament wird enteral verabreicht (=über den Magen-Darm-Trakt aufgenommen)
+ Erreicht über Pfortader die Leber
+ Wird dort teilweise verstoffwechselt und damit unwirksam gemacht
+ Dadurch kommt nicht die ganze verabreichte Dosis im Blut an => Bioverfügbarkeit sinkt
+ Beeinflussung durch: Leberfunktion, chemischen Eigenschaften des Medikaments
+ Umgehungsmöglichkeit: parenterale, sublinguale und rektale Applikation

## Barrieren

+ Blut-Hirn Schranke
+ Plazentaschranke

## Metabolisierung

= die Aufnahme, den Transport und die chemische Umwandlung von Stoffen sowie die Abgaben von Stoffwechselprodukte an die Umgebung.

## Exkretion

Der unveränderte Wirkstoff kann über die Niere, Galle, Darm, Lunge, Haut und Speichel aus dem Körper ausgeschieden werden. Wird der Wirkstoff hingegen zunächst biotransformiert und anschließend ausgeschieden, spricht man von **"Elimination"**.
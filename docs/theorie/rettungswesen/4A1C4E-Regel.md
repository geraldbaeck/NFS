title:          Was sagt die 4A1C4E-Regel aus? Wann ist sie anzuwenden?
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Rettungswesen Fragen
md5:            556ae8059eb73fa9ae9ad5e6be1a3526
date_modified:  2019/05/18
date:           2019/04/01

+ Merkregel
+ benennt Gefahrenschwerpunkte
+ bei MANV anzuwenden

+ **A**temgift
+ **A**ngstreaktion
+ **A**usbreitung
+ **A**tomare Gefahr
+ **C**hemische Stoffe
+ **E**xplosion
+ **E**insturz
+ **E**lektrizität
+ **E**rkrankung
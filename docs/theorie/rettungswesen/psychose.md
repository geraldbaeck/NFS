title:          Was ist eine Psychose? Welche Maßnahmen können sie treffen?
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Rettungswesen Fragen
md5:            13d3bd9d18d60f348863a792e112f37c
date_modified:  2019/05/12
date:           2019/04/01

+ = Überbegriff für verschiedene schwerwiegende psychische Störungen, bei denen Betroffene den Bezug zur Realität verlieren.
+ Oft verbunden mit einer Selbst- und Fremdgefährdung

+ Eigenschutz
+ Polizei / Amtsarzt

+ Respekt vor der erkrankten Person
+ Entschärfen der Situation
+ Beruhigen, Hilfe anbieten
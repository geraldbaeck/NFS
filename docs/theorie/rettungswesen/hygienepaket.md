title:          Was ist im Hygienepaket enthalten - wann ist es anzuwenden?
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Rettungswesen Fragen
md5:            014f60b30c54a36912b5a7bf73622e6f
date_modified:  2019/05/14
date:           2019/04/01

+ Allgemeine Schutzausrüstung
    + FFP 3 Filtermasken mit Ausatemventil (3 Stück)
    + Plastiksack groß (1 Stück)
+ Hygiene Schutzausrüstung
    + Einmalschürzen (3 Stück)
    + Schuhüberzüge blau (3 Paar)
    + OP – Maske grün (2 Stück) für den (infektiösen) Patienten zum Schutz gegen Aushusten
+ ABC Schutzausrüstung
    + Schutzanzug Typ 3B gelb (3 Stück)
    + Füßlinge zu Schutzanzug gelb (3 Paar)
    + Chemikalienschutzhandschuhe grün (3 Paar)
    + Schutzbrille (3 Stück)
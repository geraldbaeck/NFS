title:          Welche Einsatzarten gibt es im Rettungsdienst?
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Rettungswesen Fragen
md5:            21934cdda4efab7b62060321191481ea
date_modified:  2019/05/14
date:           2019/04/01

+ Primäreinsatz
+ gleichzeitiger Primäreinsatz
+ Folgeeinsatz (zb Nachforderung)
+ Transfer: (von KH in KH)
+ Verlegung: (von KH ins Pflegeheim)
+ Heimtransport
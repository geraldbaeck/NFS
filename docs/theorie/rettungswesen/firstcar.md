title:          Welche Maßnahmen sind zu treffen, wenn Sie als erster zu einem Großeinsatz kommen?
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Rettungswesen Fragen
md5:            c47e934b0dd2091fb55da3dc39d74957
date_modified:  2019/05/14
date:           2019/04/01

+ Sicherheit beachten (GAMS)
+ Erste Sichtmeldung abgeben
+ der ersteingetroffene NFS übernimmt die provisorische Einsatzleitung
+ analog 1. NA = LNA
+ Überblick verschaffen
+ Kontaktaufnahme mit Feuerwehr od. Polizei, je nach Einsatzlage
+ Lagebericht abgeben
+ Grenze der Gefahrenzone gemeinsam mit Polizei/FW festlegen
+ Räume festlegen
+ Bildung der Patientenablage (PLS ausgeben)
+ Einsatz übergeben
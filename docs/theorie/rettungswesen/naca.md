title:          NACA-Einteilung
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Rettungswesen Fragen
md5:            c66a58991fae7545b56ebbec037b5b08
date_modified:  2019/04/01
date:           2019/04/01

Scoring-System, um die Schwere von Verletzungen, Erkrankungen oder Vergiftungen in der Notfallmedizin zu beschreiben. Es wurde vom namensgebenden ***National Advisory Committee for Aeronautics*** ursprünglich als Einsatzbewertung im Hinblick auf Unfälle in der Luftfahrt entwickelt.

| Grad | Beschreibung | Beispiele |
| --- | --- | --- |
| 0 | Keine Verletzung oder Erkrankung. Diese Kategorie wird häufig entweder ersatzlos gestrichen oder durch NACA I ersetzt. | Fehleinsatz |
| I | Geringfügige Verletzung bzw. Funktionsstörung. In der Regel keine notärztliche Intervention erforderlich. | Prellung,leichte Hautabschürfung |
| II | Leichte bis mäßig schwere Funktionsstörung. Ambulante ärztliche Abklärung bzw. Therapie, in der Regel aber keine notärztlichen Maßnahmen erforderlich. | Fraktur eines Fingerknochens, mäßige Schnittverletzungen; Verbrennung II. Grades |
| III | Mäßige bis schwere, aber nicht lebensbedrohliche Störung. Stationäre Behandlung erforderlich, häufig auch notärztliche Maßnahmen vor Ort. | Offene Wunden; Oberschenkelfraktur; leichter Schlaganfall; Rauchgasvergiftung |
| IV | Schwere Störung, bei der die kurzfristige Entwicklung einer Lebensbedrohung nicht ausgeschlossen werden kann; in den überwiegenden Fällen ist eine notärztliche Versorgung erforderlich. | Wirbelverletzung mit neurologischen Ausfällen; schwerer Asthmaanfall; Medikamentenvergiftung |
| V | Akute Lebensgefahr. Transport in Reanimationsbereitschaft | schwerer Herzinfarkt; erhebliche Opioidvergiftung |
| VI | Atem- und/oder Kreislaufstillstand (kardiopulmonale Reanimation erforderlich). | Herzstillstand, Kammerflimmern |
| VII | Tödliche Verletzung oder Erkrankung | Erfolglose Reanimation oder Todesfeststellung |
title:          GAMS
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Rettungswesen Fragen
md5:            eda0bd0ce77f3ad08a3e00b22ba0744c
date_modified:  2019/04/01
date:           2019/04/01

=beschreibt das richtige Verhalten bei Gefahr

+ **G**efahr erkennen
+ **A**bstand halten, falls keine ausreichende Absicherung möglich ist
+ **M**enschenrettung durchführen (falls gefahrlos möglich)
+ **S**pezialkräfte anfordern und diese vorab informieren

## 3A Regel

+ **A**bstand - so groß wie möglich
+ **A**ufenthaltszeit - so kurz wie möglich
+ **A**bschirmung - so gut wie möglich
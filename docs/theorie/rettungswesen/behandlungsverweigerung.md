title:          Der Patient will sich nicht behandeln lassen. Welche Maßnahmen können sie treffen?
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Rettungswesen Fragen
md5:            161839343b3d00702cfc6a7fa5fdca3c
date_modified:  2019/05/12
date:           2019/04/01

+ Patient einsichts- und urteilsfähig
    + Behandlung kann abgelehnt werden
+ nicht einsichts- und urteilsfähig
    + evt. Notarzt
    + Polizei & Amtsarzt entscheidet über Maßnahmen nach dem UBG
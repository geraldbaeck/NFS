title:          Wie findet man die "kritischen" Patienten bei einem Großeinsatz? Was versteht man darunter?
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Rettungswesen Fragen
md5:            a94d5f33493ef92f68e8bf2882a84882
date_modified:  2019/05/30
date:           2019/04/01

+ Triagegruppe I = lebensbedrohlicher Zustand, der durch einen Soforteingriff gebessert werden kann.
+ Triagegruppe II = hohe Transportpriorität bedürfen, da vor Ort keine Hilfe geleistet werden kann
+ Triagegruppe IV =Behandlungen, die mit den vorhanden Ressourcen nur sehr erschwert behandelt werden können und somit abwartend und betreut zu betrachten sind
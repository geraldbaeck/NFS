title:          GAMS Regel: M Wann kann der Rettungsdienst die Rettung aus der Gefahrenzone machen - und wann nicht?
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Rettungswesen Fragen
md5:            92b970edd2fb09955e374b8a491c7ddb
date_modified:  2019/05/14
date:           2019/04/01

+ Bergung aus Gef.Zone ist in erster Linie Aufgabe der Feuerwehr
+ Bei Freigabe durch die Feuerwehr oder Polizei, je nach Gefahrenlage
    + Wenn – sichergestellt ist, dass kein gefährlicher Stoff ausgetreten bzw. wenn es sich um einen gering gefährlichen Stoff handelt
    + Wenn – keine Gefahr durch einen Täter ausgehen kann – bzw. der Täter gestellt ist.
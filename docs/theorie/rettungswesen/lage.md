title:          Was ist die Lagemeldung und was ist der Lagebericht?
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Rettungswesen Fragen
md5:            eae05ba4855738410c78dc81a6bf13fb
date_modified:  2019/04/01
date:           2019/04/01

## 1. Lagemeldung

=Sichtmeldung

anschließend aussteigen, Überblick verschaffen, mit anderen Organisationen vor Ort Kontakt aufnehmen

## Lagebericht

+ genaue Ortsangabe
+ Was ist passiert?
+ Gefahrenlage?
+ ungefähre oder zu erwartende Dimension
+ Anzahl der Verletzten / Schwerverletzten
+ Zufahrtswege
+ etwaige Besonderheiten

anschließend Verbindung halten
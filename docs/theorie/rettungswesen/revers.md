title:          Was ist ein Revers? Was sind die Voraussetzungen dafür?
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Rettungswesen Fragen
md5:            e492886df2bbcf3f77a95b0d08f4c6ae
date_modified:  2019/05/30
date:           2019/04/01

## Definition

+ = allgemein ein „Verpflichtungsschein“
+ kommt gesetzlich nicht vor

In der Medizin wird der Begriff Revers im Rahmen der dauerhaften oder zeitweiligen Verweigerung einer medizinischen Behandlung verwendet.

## Voraussetzungen

+ >= 14a
+ Patient einsicht- und urteilsfähig
+ Aufklärung über die möglichen gesundheitlichen Folgen
    + nachdrücklich
    + in verständlicher Form

## Inhalt

Der Revers ist eine schriftlich unterzeichnete Dokumentationsform mit der ein Patient

+ ablehnt:
    + Versorgung
    + Transport
+ bestätigt:
    + ordnungsgemäße Aufklärung als Voraussetzung für die Willensbildung
    + über die möglichen Nachteile durch die Verweigerung
    + Hinweiß zur Eigenverantwortung

## Zweck

+ rechtliche Absicherung für den Rettungsdienst
+ nochmalige Möglichkeit für den Patient, seine Weigerung zu überdenken
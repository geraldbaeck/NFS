title:          Wann kann der Amtsarzt angefordert werden und wofür?
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Rettungswesen Fragen
md5:            9dd599937e7b5bf97d4e54cf227d37dc
date_modified:  2019/05/14
date:           2019/04/01

+ Patient nicht einsichts- und urteilsfähig
+ Nichtbehandlung hätte ernstliche und erhebliche Folgen für die Gesundheit oder das Leben des Patienten
+ ist ein Amtsarzt hinzuzuziehen welcher über Maßnahmen nach dem UBG entscheidet
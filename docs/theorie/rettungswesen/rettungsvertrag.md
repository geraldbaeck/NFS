title:          Was ist ein Rettungsvertrag und wie kommt er zustande?
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Rettungswesen Fragen
md5:            63396dd8f9c41da09a206d5ae79c3ee4
date_modified:  2019/06/04
date:           2019/04/01

## Definition

+ regelt das Verhältnis zwischen Patient und RD
+ SanitäterInnen werden als Erfüllungsgehilfen des RD tätig => nicht Vertragspartner
+ der Vertrag kommt durch
    + Stillschweigen,
    + [konkludentes (=schlüssiges) Verhalten](https://de.wikipedia.org/wiki/Schl%C3%BCssiges_Handeln)
    + ausdrückliche Zustimmung
+ Voraussetzungen:
    + PatientIn muss ansprechbar sein
    + muss handlungsfähig (volljährig) sein
    + muss seinen Willen äußern können

Kein Vertrag kommt zustande bei:

+ bei bewusstlosen Notfallpatienten => handeln als "Geschäftsführer im Notfall" §1036 ABGB
+ bei Minderjährigen und bei Personen, die nicht in der Lage sind, ihren Wille frei zu äußeren
+ bei unmündigen Minderjährigen wird der Rettungsvertrag mit einem Elternteil geschlossen
+ mündige Minderjährige dürfen selbst entscheiden, wenn es sich um keine lebensnotwendige Behandlung handelt
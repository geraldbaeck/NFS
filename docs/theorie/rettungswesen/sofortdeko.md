title:          Was ist eine Sofort-Dekontamination und wer macht sie?
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Rettungswesen Fragen
md5:            64ee76380616eb9e8aabfff4089cb054
date_modified:  2019/05/14
date:           2019/04/01

+ = Entfernung von kontaminierter Kleidung
+ = Abwaschen der Haut

+ Feuerwehr, SEG-ABC, CBRN
title:          Was sind sichere Todeszeichen?
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Rettungswesen Fragen
md5:            c0e7d137773c42285537c037f38e024b
date_modified:  2019/06/02
date:           2019/04/01

+ Mit dem Leben nicht vereinbare Verletzungen (Kopfabtrennungen, totale Deformation)
+ Totenflecken
+ Totenstarre: 1-2h Stunden, löst sich nach 2-3d
+ Verwesungserscheinungen
    + Leichengeruch
    + Fäulnis
    + aufgetriebener Bauch
    + Ausfluss von übelriechender Flüssigkeit aus dem Körper
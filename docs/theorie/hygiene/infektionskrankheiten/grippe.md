title:          Grippe
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Hygiene
                Notfallmedizin
md5:            8143d0d6c62d736e744816316bb59090
date_modified:  2019/06/12
date:           2019/04/04

## Definition

+ durch das Influenzavirus verursachte Erkrankung der Atemwege
+ greift die Schleimhaut (Mucosa) der Atemwege an
+ erleichtert das Eindringen anderer Erreger

## Übertragung

+ Tröpfcheninfektion
+ vereinzelt Schmier- und Kontaktinfektion möglich
+ Inkubationszeit 1-4d

## Symptome

treten meist plötzlich auf

+ Hohes Fieber
+ Schüttelfrost
+ Gliederschmerzen
+ Husten
+ Schnupfen
+ Kreislaufschwäche
+ Entzündung der oberen Atemwege

## Komplikationen

+ perakute Viruspneumonie bei bestimmten Stämmen möglich
+ Myokarditis
+ Grippeotitis
+ Grippeenzephalitis
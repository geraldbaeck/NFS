title:          Tuberkulose (TBC)
date:           2019/01/06
version:        0.0.7
authors:        gerald@baeck.at
ankikat:        Hygiene
                Notfallmedizin
md5:            9675389a2f24f371ddbd030dc1753fed
date_modified:  2019/06/18

## Definition

+ bakterielle Infektionskrankheit (**mykobakterium tuberculosis**)
+ Tröpfcheninfektion von Mensch zu Mensch va über die Lunge
+ Tier auf den Menschen
  + Rindertuberkoluse über Milch (selten, mykobakterium bovis)
  + Geflügeltuberkulose (häufiger, va bei HIV PatientInnen atypische TBC, mykobakterium avium)
+ verläuft oft ohne charakteristische Beschwerden
+ chronisch und akut möglich
+ Kann einzelne Organe oder gesamten Organismus betreffen
+ Inkubationszeit 4-6 Wochen

**Geschlossen**: keine Keimabgabe an die Umgebung  
**Offen**: Keimabgabe an Umgebung

## Symptome

+ Primärkomplex
    + Primärherd (meist die Lunge)
    + und die befallenen Lymphknoten
    + Abszesshöhlen in der Lunge
    + Wird diese abgekapselt und verkalkt, Spricht man von einem Tuberkelreservoir
+ uncharakteristische Symptome
    + Husten
    + Gewichtsverlust
    + Fieber
    + Nachtschweiß
    + leichte Ermüdbarkeit
+ betroffene Thoraxhälfte wird beim Atmen sichtbar geschont
+ auskultierbares [Pleurareiben](https://www.youtube.com/watch?v=CFsfb8Mv38k)

## Komplikationen

+ Dyspnoe (Pleuraerguss, [Kavernen](https://de.wikipedia.org/wiki/Kaverne_(Medizin)))
+ [Meningitis](meningitis.md)
+ Pleuritis (Brustfellentzündung)
+ Peritonitis (Bauchfellentzündung)
+ Knochentuberkulose
+ [Hämoptyse (Bluthusten)](/theorie/notfälle/kardiovaskulär/bluthusten-haemoptyse/)
+ Sepsis

## Anamnese

+ Gibt es Tuberkolusefälle im Umfeld?

## Maßnahmen

+ Eigenschutz (PSA, Handschuhe, FFP3-Maske)
+ Exp.prophylaxe Typ 2
+ bei offener TBC Mundschutz für Patienten
+ Prophylaktische Impfung wird derzeit nicht empfohlen

+ Nach dem Transport:
    + Gründliches Lüften des Fahrzeuges
    + Gründliche Wischdesinfektion
    + Handschuhe, Masken in eigenem Müllsack entsorgen
title:          Inkubationszeiten
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Hygiene
                Notfallmedizin
md5:            bf4b428ceef8cb9c7eba42f021f9a0bb
date_modified:  2019/06/07
date:           2019/06/07

| Krankheit | Inkubationszeit |
| --- | --- |
| Streptokokken | h-d |
| Grippe | h-d |
| Pneumonie | h-d |
| Gonorrhoe | einige Tage |
| Hepatitis A | 4-6w |
| Hepatitis B | 2-6m |
| Hepatitis C | 15-150d |
| Tuberkulose | 4-6w |
| HIV | bis 20y |
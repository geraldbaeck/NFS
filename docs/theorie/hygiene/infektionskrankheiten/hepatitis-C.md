title:          Hepatitis C
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Hygiene
                Notfallmedizin
md5:            22948001cdb250a5cb9efeec84e59702
date_modified:  2019/04/21
date:           2019/04/20

## Definition

Der Erreger ist das Hepatitis C-Virus

+ empfindlicher als das Hepatitis B Virus
+ Hepatitis C ist der häufigste Grund für eine Lebertransplantation!

## Übertragung

+ Blut
+ Infizierte Körperflüssigkeiten
+ Infizierte Blutkonserven
+ Intravenöser Drogenkonsum mit Nadeltausch
+ kaum durch Geschlechtsverkehr
+ Inkubationszeit liegt bei 15-150 Tage

## Symptome

+ Akuter Verlauf
    + Müdigkeit
    + wie bei Grippe
    + symptomlos
+ Chronischer Verlauf (80%)
    + Leberzirrhose
    + Leberkrebs

## Maßnahmen / Prophylaxe

+ KEINE Schutzimpfung
+ Hygienische Händedesinfektion
+ Wischdesinfektion
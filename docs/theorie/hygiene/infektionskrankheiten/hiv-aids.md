title:          HIV, AIDS
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Hygiene
                Notfallmedizin
md5:            1fc83b4e63d6bd16579e54e64ecb6601
date_modified:  2019/04/21

## Definition

+ Erreger: Human Immunodeficiency Virus
+ zwei Haupttypen und mindestens 15 Untertypen

+ befällt Helfer-T-Lymphozyten und Makrophagen, hemmt deren Funktion oder tötet sie
+ wesentlicher Teil der Immunabwehr fällt aus

+ sehr empfindlich gegenüber Umwelteinflüssen
+ hohe minimale Infektionsdosis (höher als bei Hepatitis-B)
+ außerhalb des Körpers (eingetrocknetes Blut) ist das Virus nach 30 Minuten nicht mehr infektionsfähig
+ verträgt Hitze und Desinfektionsmittel schlecht

+ Frühestens nach drei Wochen sind Antikörper nachweisbar, (diagnostisches Fenster) PCR Test nach 14 Tagen
+ AIDS = Ausbruch der Krankheit
+ Inkubationszeit bis zu 20 Jahre

## Übertragung

+ Übertragung durch GV, Blutwege oder pränatal
+ Körperflüssigkeit mit ausreichend Erregern gelangt in die **Blutbahn**
+ Viren lagern sich in den Abwehrzellen an (T-Helferzellen).
+ Bereits nach 20 Minuten sind sie so integriert, dass sie unangreifbar sind

## Verlauf

+ HIV Positiv = Latente Infektion
+ Symptomfreier Intervall
+ AIDS Vorstadium: Lymphknotenschwellung, Fieber, Durchfälle, Nachtschweiß, Gewichtsverlust
+ Monate bis Jahre
+ 30% aller HIV positiven erkranken an AIDS
+ AIDS Vollbild: viele Infektionen, Tumore, Degenerative Hirnerkrankungen, Abbau höherer Hirnleistung, 100% tödlicher Verlauf

## Vorbeugung

+ nur geschützter GV
+ Einmalspritzen
+ Nadelstichverordnung
+ Verletzungen vermeiden (bester Tipp:)
+ PSA Einmalhandschuhe
+ Wischdesinfektion
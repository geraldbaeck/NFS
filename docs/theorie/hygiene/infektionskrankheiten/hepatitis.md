title:          Hepatitis
date:           2018/12/28
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            67e1f7285420809fd86d35ae147eea07
date_modified:  2019/01/07

| Hepatitis | A | B | C |
| --- | --- | --- | --- |
| `Definition` | Übertragung fäkaloral, Virus verursacht Entzündung der Leber | Übertragung Blut\Sekrete, Virus verursacht chronische Entzündung der Leber => Leberkarzinom | Übertragung Blut\Sekrete, Virus verursacht chronische Entzündung der Leber |
| `Vorbeugung` | Aktive Immunisierung | Aktive Immunisierung | Keine |
| `Symptome` | Durchfall, Schwäche, Erbrechen, __Gelbsucht__, Fieber | Schwäche, Erbrechen, Gelbsucht, Fieber | Schwäche, Erbrechen, Gelbsucht, Fieber |
| `Komplikationen` | Selten akutes Leberversagen | akutes LBV, selten chr. Hepatitis | chr. Hepatitis, Leberzirrhose, -karzinom |
| `Exp.prophylaxe` | Typ 1 | Typ 1 | Typ 1 |
title:          Streptokokken Infektionen
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Hygiene
                Notfallmedizin
md5:            7dbbc1a5570cfd64cfa54261dd5ad5ef
date_modified:  2019/04/21
date:           2019/04/20

## Definition

+ in Ketten liegende Kugelbakterien
+ krank- und nicht krankmachende Vertreter
+ Krankmachende Streptokokken, produzieren Toxine (=Enzyme) mit bestimmter Wirkung wie:
    + Auflösen von roten Blutkörperchen
    + Auflösen von frischen Blutgerinsel
    + Erweichung von Bindegewebe

## Übertragung

+ Schmierinfektion
+ Tröpfcheninfektion
+ Inkubationszeit Stunden bis Tage

## Symptome

+ flächenhafte Eiterung im (tiefer) liegendem Gewebe (Phlegmone)
+ aber auch, akutes rheumatisches Fieber mit einer Entzündung von Gelenken

## Komplikationen

+ starke Wundinfektion => Sepsis
+ Sonderfall: **Eitrige Angina**
    + Tröpfcheninfektion
    + eitrige Entzündung im Rachenraum und Mandeln
    + hohes Fieber
    + Absteigen des Keimes bis zu den Herzklappen möglich
        + Vernarbung => Herzklappeninsuffizienz => Herztransplantation
+ Sonderfall: **Scharlach**
    + Tröpfcheninfektion
    + Fieber
    + Müdigkeit
    + Hautausschlag
    + Himbeerzunge
    + Komplikationen: Herzklappenentzündung, Nierenentzündungen
+ Sonderfall: **Rotlauf/Erysipel**
    + Kontaktinfektion
    + hohes Fieber
    + Schmerzen
    + scharf begrenzten Entzündung
    + Komplikation: Elephantiasis

## Prophylaxe

+ Einmalhandschuhe
+ Hygienische Händedesinfektion
+ Gründliche Wischdesinfektion
+ Eventuelle Antibiotika Prophylaxe
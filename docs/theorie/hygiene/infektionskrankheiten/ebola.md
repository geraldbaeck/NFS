title:          Ebola
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            68f1e203f45d49213f1a1ce3a62bde98
date_modified:  2019/01/07

Übertragung durch Kontakt- und Tröpfcheninfektion. Infektionsgebiet in Westafrika.

## Symptome
+ Hohes Fieber
+ Starke Kopfschmerzen
+ Erbrechen, Durchfall
+ Bauchschmerzen
+ Unerklärliche Blutungen

## Komplikationen
+ Multiorganversagen
+ Tod

## Maßnahmen
+ Transport durch HRI Team
title:          Hygienische Händedesinfektion
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            dc86f4945c7b5127cb26cca84de29a5b
date_modified:  2019/01/07

## Definition
Verfahren zur Verringerung der Zahl von Krankheitserregern auf der Haut der Hände mit Hilfe von Händedesinfektionsmitteln. 

## Ziele
+ Vermeidung der Übertragung von Krankheitserregern (insbesondere Bakterien und Viren) von einem Patienten zum nächsten (Kreuzinfektion)
+ Eigenschutz

## Transiente Hautflora
+ Desinfektion dient der Reduzierung der transienten Hautflora = die vorübergehende Besiedelung mit hautfremden, möglicherweise krankheitsverursachenden (pathogenen) Erregern

## Residente Hautflora
+ schützt vor krankmachenden Erregern
+ wird von Desinfektionsmitteln angegriffen wird
+ => gehört zur Händedesinfektion auch der systematische Aufbau eines Hautschutzes.

## Wann? aka Fünf-Momente-Konzept
| VOR | NACH |
| --- | --- |
| <ul><li>Patientenkontakt</li><li>aseptischen Tätigkeiten</li></ul> | <ul><li>Kontakt mit potenziell infektiösem Material (Stuhl, Körperflüssigkeiten)</li><li>Patientenkontakt</li><li>Kontakt mit Oberflächen in unmittelbarer Umgebung des Patienten</li></ul> |

Hygieneplan beachten
{: .info}

## Ablauf
+ mechanische Reinigung
+ Desinfektionsmittel aufbringen (2-3 Hübe)
+ Handflächen
+ Handrücken
+ Handgelenke
+ Unterarme ggf
+ Fingerzwischenräume
+ Finger
+ Daumen
+ Fingerkuppen

Mindesteinwirkzeit 30 Sekunden
{: .info}

![Hygienische Händedesinfektion](https://upload.wikimedia.org/wikipedia/commons/a/ae/04_Hegasy_Hand_Hygiene_Wiki_DE_CCBYSA.png)
Hygienische Händedesinfektion[^1]
{: .imagewithcaption}

[^1]: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:04_Hegasy_Hand_Hygiene_Wiki_DE_CCBYSA.png), [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en)
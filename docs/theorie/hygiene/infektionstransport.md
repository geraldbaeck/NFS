title:          Infektionstransport
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            d9d205e4c7dae37054840753bc3683ee
date_modified:  2019/06/04

## Vorbereitung

+ Schiebefenster zum Fahrer schließen, mit Klebeband sichern um gedankenloses Öffnen zu erschweren
+ Schreibmöglichkeit vorbereiten zur Kommunikation mit FahrerIn
+ Belüftung abschalten, Lüftungsauslässe verkleben
+ Patientenraum möglichst leeren

## PSA anlegen

FahrerIn leitet die beiden anderen SanitäterInnen an.

## Tranportdurchführung

+ Kontakt mit dem BO-Krankenhaus aufnehmen und Details der Übergabe besprechen
+ PSA ist bereits vor dem ersten Patientenkontakt voll angelegt

+ FahrerIn schützt sich am BO durch Einmalhandschuhe, FFP3 Maske und Abstand zum Patienten
+ FahrerIn ist für das Öffnen und Schließen von Türen, Kommunikation und Dokumentation zuständig
+ Vor Öffnen des Fahrerraum, PSA ablegen und hyg Händedesinfektion

+ Patient erhält FFP2 Maske ohne Ausatemventil oder eine OP Maske
+ auf wertschätzenden Umgang achten

+ Bei offensichtlicher Kontamination (Anhusten, Blutspritzer) Stelle sofort desinfizieren und kontaminiertes Material am AO übergeben

+ Am AO durch Fahrer anmelden und Details der Übergabe klären

## Nachbereitung

+ SanitäterInnen bleiben im Patientenraum
+ An der Dienststelle Desinfektion durchführen erst danach PSA ablegen
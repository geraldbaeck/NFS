title:          Postexpositionsprophylaxe
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            563aa6d29a426a4b616b72c6aa28c323
date_modified:  2019/01/07

## Definition
+ nach erfolgter Kontamination, um Ausbrechen der Krankheit zu verhindern
+ z.B.: Schnittverletzungen, Hautexposition, Schleimhautexposition, Krankenanstalt, HIV

## Maßnahmen
+ Ausbluten/Ausdrücken der Wunde min. 1 Min.
+ danach mit virusinaktivierendem Antiseptikum auf Mulltupfer benetzen.
+ Mull fixieren und immer wieder benetzen mind 10 min


+ **Schnittverletzungen**: Blutfluss durch Spreizen der Wunde verstärken. Danach antiseptische Spülung mit alkohol. Hautdesinfekt
+ **Hautexposition**: sofortiges Entfernen des potenziell infektiösen Materials mit alkoholgetränkten Tupfer. Danach Abreiben der Hautoberfläche mit großzügiger Einbeziehung des Umfelds um sichtbar kontaminiertes Areal.
+ **Schleimhautexposition**: sofort und ausgiebig spülen
+ **Weitergehende Maßnahmen**: im Anschluss an Sofortmaßnahmen ist KH aufzusuchen. Verletzung dokumentieren und entsprechend den lokalen Regelungen melden.
+ **Postexpositionelle HIV-Prophylaxe (PEP)**: innerhalb von 1-2 Std, aber innerhab von 48-72 Std jedenfalls beginnen
+ **Postexpositionelle Meningitis-Prophylaxe**: Antibiotika
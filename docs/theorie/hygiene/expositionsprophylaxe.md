title:          Expositionsprophylaxe
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            b296d152b334fbdab30007ee1550a987
date_modified:  2019/01/07

##  Definition
Vorbeugende Maßnahmen bei Infektionspatient z.B.: Stuhl & Blut – für medizinisches Personal existiert Gruppe 1 nicht.

## Expositionsprophylaxe Typ 1
Bei möglichen Kontakt mit Körpersekreten oder Ausscheidungen

+ Handschuhe
+ Desinfektion, Händedesinfektion
+ ggf. Einmalschürze
+ sichere Abfallentsorgung

## Expositionsprophylaxe Typ 2
Bei Verdacht auf Tröpfcheninfektion oder aerogene Übertragung, zusätzlich zu Typ 1:

+ Mundschutz für Patienten
+ nachher Auto lüften
+ Oberflächendesinfektion

## Expositionsprophylaxe Typ 3
Bei Verdacht auf Erkrankungen wie Milzbrand, Lungenpest

+ Schutzausrüstung entsprechend Hygieneset (FFP3 Maske, Schutzbrille, Overall Klasse CE Kat3, Überschuhe, Handschuhe)
+ Schlussdesinfektion (legt der Amtsarzt fest)
title:          Schlaganfall
date:           2019/01/06
version:        0.0.5
authors:        gerald@baeck.at
ankikat:        Notfälle Cerebral
                Notfallmedizin
md5:            86eb085c3e482845ab17dd509836c023
date_modified:  2019/06/02

## Definition

Plötzliches Auftreten von neurologischen Ausfällen, kann durch Durchblutungsstörung oder eine Blutung im Hirn ausgelöst werden.

## TIA (transitorisch ischämische Attacke)

+ =flüchtige Durchblutungsstörungen an
+ 90% < 10 Minuten
+ Ausfallserscheinungen bilden sich innerhalb von 24 Stunden vollständig zurück
+ > 24h = Progredientes Reversibles Ischämisches Neurologisches Defizit (PRIND)

Im Akutstadium (solange die Symptome noch bestehen) kann zwischen einer TIA und einem Schlaganfall nicht unterschieden werden. Die Akutbehandlung muss sich deshalb am Vorgehen beim Schlaganfall orientieren, insbesondere ist eine rasche Diagnose zwingend.

## Insult (Ischämischer Insult)

+ = Sauerstoffmangel aufgrund von plötzlichen Durchblutungsstörung
+ Minderdurchblutung verursacht ein Absterben von angeschlossenen Nerven- und Bindegewebe

## Ursachen

+ Thromboembolisches Geschehen: durch losgelöste arterosklerotische Plaques aus großen Hirnarterien oder Thromboembolien, die Ursprung im Herz haben (zB bei Vorhofflimmern).
+ [Hämodynamisch](https://de.wikipedia.org/wiki/H%C3%A4modynamik) verursachte Infarkte: bei Engstellen von zuführenden Gefäßen außerhalb des Gehirns (selten)
+ Spasmus

## Hirnblutung (hämorrhagischer Insult)

+ =spontane Zerreißung von Hirngefäßen und Einblutung ins Gewebe
+ => Erhöhung des Hirndrucks
+ => Behinderung des venösen Abflusses

Eine Unterscheidung zwischen Ischämischen und Hämorrhagischen Insult kann nur durch ein CT getroffen werden, deshalb kein Unterschied in der Versorgung.

## Leitsymptome

+ Halbseitenzeichen
    + Schwäche (Parese)
    + Lähmung (Plegie)
    + Gefühlsstörung (Parästhesie)
+ Akute einseitige Sehstörung
+ Akute Sprechstörung
    + Motorische Aphasie
    + Sensorische Aphasie

## Unspezifische Symptome

+ Kopfschmerz
+ Übelkeit
+ Erbrechen
+ Schwindel
+ Bewusstlosigkeit
+ Herdblick

## Komplikationen

+ Bewusstlosigkeit
+ Aspiration

## Maßnahmen

+ Allgemeine Maßnahmen
+ Prophylaktische Seitenlage
+ erhöhter Oberkörper (30%)
+ Absaugbereitschaft
+ rascher Transport (time is brain!) => stroke unit
+ O&#8322;
+ Anamnese
+ BZ Test!!!!

## Anamnese

+ Immer abklären: Neue oder alte Symptomatik
+ Ereigniszeitpunkt, möglichste genau, wichtig für Lyse < 4.5h
+ Vorerkrankungen: Zucker, Bluthochdruck, Vorhofflimmern, Herzklappenerkrankungen
+ Medikamente, va Blutverdünner

![Kurzwiederholung Apoplex/Schlaganfall/Hirninfarkt/Ischämischer Insult](https://www.youtube.com/watch?v=_GP93m6QJw4)
![Ischämischer Schlaganfall I](https://www.youtube.com/watch?v=QniNVDOPdPo)
![Ischämischer Schlaganfall II](https://www.youtube.com/watch?v=MPUFU12R7RI)
![Subarachnoidalblutung](https://www.youtube.com/watch?v=ucWF8AwS_H8)
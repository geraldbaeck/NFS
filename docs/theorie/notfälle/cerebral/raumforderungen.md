title:          Intrakranielle Raumforderungen
version:        0.0.5
authors:        gerald@baeck.at
ankikat:        Notfälle Kardiovaskulär
                Notfallmedizin
md5:            8f648ed45860ec46c99f294cfe6347e4
date_modified:  2019/06/06
date:           2019/04/01

## Definition

+ Gehirn ist fest vom Schädelknochen umgeben => keine Ausdehnung möglich ("rigid box")
+ Intrakranielles Volumen ist konstant bei ca. 1600ml
    + 80% Gehirngewebe (1200ml)
    + 12% Blut (240ml)
    + 8% Liquor (160ml)
+ Monro-Kellie-Doktrin
    + Summe der drei Komponenten muss gleich bleiben
    + Zunahme einer Komponente führt zur Abnahme der anderen
    + zb chronische Liquorstörung => Hirnathropie
    + zb Hirnödem => Liquorabnahme
+ Volumensanstieg steigert den Hirndruck => evt. Einklemmen des Hirnstammes

## Ursachen

+ Blutung
    + Epiduralblutung
    + Subarachnoidalblutung (SAB) = Zerreißen eines erweiterten Blutgefäßes (Aneurysma) im Subarachnoidalraum
        + Heftigste akute Kopfschmerzen
        + Meningismus (Nackensteifigkeit)
        + Bewusstseinsverlust
        + Manchmal Krampfanfälle
        + hoher RR
    + Intracerebrale Blutung = In der Gehirnmasse selbst kann es durch Gefäßzerreissungen oder Tumore zu Blutungen kommen.
+ Tumor (Metastasen)
+ [Hirnödem (Hirnschwellung)](https://de.wikipedia.org/wiki/Hirn%C3%B6dem)
+ Hydrocephalus (Wasserkopf) = Missverhältnis von Produktion und Abfluss des Liquor cerebrospinalis bzw. Abflusshindernis

## Symptome

+ Übelkeit
+ Kopfschmerz
+ Bewusstseinseintrübung
+ Krämpfe
+ Pupillenveränderungen ([Stauungspapille](https://de.wikipedia.org/wiki/Stauungspapille))
+ Augenmuskellähmung
+ bei Säuglingen: Verdrehen des Augapfels nach unten (Sonnenuntergangs-Phänomen)

## Komplikationen

+ Hypoperfusion (Perfusionsdruck = MAP - intrakranieller Druck; >65mmHg)
    + [Prinzip der letzten Wiesen](https://flexikon.doccheck.com/de/Prinzip_der_letzten_Wiese)
    + =Zellen im Endstromgebiet sterben zuerst ab
+ Einklemmung
    + Tentorium cerebelli (Kleinhirnzelt)
    + Hirnstammes im Hinterhauptloch (Foramen magnum)

## Maßnahmen

+ Allgemeine Maßnahmen bei Bewusstlosigkeit
+ Oberkörper hoch
+ O&#8322;
+ Monitoring
+ NEF

[Neurologie ─ Intrakranieller Druck](https://www.youtube.com/watch?v=N_R-mKArknQ){: .youtube}
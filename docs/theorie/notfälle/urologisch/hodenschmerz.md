title:          Akuter Hodenschmerz
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Urologisch
                Notfallmedizin
md5:            f8f030939047cb16127760c915dba68c
date_modified:  2019/05/08
date:           2019/05/08

## Definition

+ Entzündung (Orchitis)
+ Hodentorsion (Verdrehung)
    + = akute Stieldrehung mit Unterbrechung der Blutzirkulation

## Symptome

+ plötzlich auftretende starke Schmerzen
+ Hodensack an der entsprechenden Seite gerötet
+ Hoden angeschwollen
+ Hoden steht evtl. hoch
+ oft bei Säuglingen

## Komplikationen

+ nach vier bis sechs Stunden bleibende Schäden
+ Hodenverlust

## Maßnahmen

+ Hospitalisierung
title:          Nierenkolik
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Urologisch
                Notfallmedizin
md5:            482cb09ccc5ef2c10711f9f4690384e5
date_modified:  2019/05/14
date:           2019/05/08

## Definition

= krampfartiger Schmerz (Kolik) in der Lendengegend oder Bauchraum (Abdomen)

## Ursachen

+ Steine/Grieß im ableitenden Harnsystem (Nierenbecken, Harnleiter, Blase, Harnröhre)
+ => hydrostatischer Druck oberhalb des Hindernisses nimmt zu
+ => Dehnungs- & Schmerzrezeptoren werden aktiv

## Symptome

+ wellen-/krampfartigen Schmerzen
+ Schmerzen in den Flanken lokalisiert, ausstrahlend in Leisten und Blasengegend
+ Schweißausbruch
+ Übelkeit
+ ↓RR, ↑HF
+ Harn möglicherweise rötlich, dunkel => Blutbeimengung

## Komplikationen

+ Nierenschädigung
+ septische Harnstauungsniere

## Maßnahmen

+ NA: schmerz-/krampflösende Medikamente, Nitro
+ Flüssigkeitszufuhr
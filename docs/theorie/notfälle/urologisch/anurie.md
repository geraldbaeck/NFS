title:          Anurie
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle Urologisch
                Notfallmedizin
md5:            5f800a0d8a578054ac2080e634cd2fc8
date_modified:  2019/05/14
date:           2019/05/08

## Definition

+ = Unterschreitung der altersüblichen physiologischen Urinmenge
+ = < 100ml / d
+ Oligurie: < 500ml / d

## Ursachen

+ prärenale Anurie (zb durch Minderdurchblutung der Niere, Akutes Nierenversagen bei Schockgeschehen)
+ renale Anurie (zB Niereninsuffizienz)
+ postrenale Anurie (durch Abflussbehinderungen zB bei Urolithiasis, Urothelkarzinom, Fehlbildungen des Ureters)

## Komplikationen

+ kann zu irreversibler Nierenschädigung führen

=> rasche Volumenzufuhr beim Schock sehr wichtig!
{: .info}
title:          Vorhautverengung
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle Urologisch
                Notfallmedizin
md5:            da70d8da57a7bd3f698fb7ddf80ac32d
date_modified:  2019/05/08
date:           2019/05/08

## Definition

+ Phimose
    + verengte Vorhaut
+ Paraphimose
    + gewaltsam zurückgezogene verengte Vorhaut
    + kann nicht mehr reponiert werden
    + => gestörter Blutfluss, schmerzhafter Stau
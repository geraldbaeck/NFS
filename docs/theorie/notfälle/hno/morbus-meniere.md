title:          Morbus Meniere
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle HNO
                Notfallmedizin
md5:            86af0fbdcc6eaa303d76673980979600
date_modified:  2019/05/01
date:           2019/05/01

## Definition

Erkrankung des Innenohres mit dem Gleichgewichtsorgan

## Symptome

Anfallartiges Auftreten von:

+ Menière’sche Trias:
    + Drehschwindel
    + Hörverlust (Hörsturz)
    + Geräusche (Tinnitus)
+ Übelkeit

Dauer Minuten bis Stunden ohne Vorwarnung

## Maßnahmen

+ Transport: HNO
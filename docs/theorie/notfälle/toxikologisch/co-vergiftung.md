title:          CO Vergiftung
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle toxikologisch
                Notfallmedizin
md5:            d7d899ff4af20371ffd45c7f0652f93f
date_modified:  2019/05/10

## Definition

+ geruchs-, geschmacks- u.farbloses Gas
+ CO blockiert das Hämoglobin für den Sauerstofftransport
+ ca 300 mal fester an das Hb gebunden als O&#8322;
+ entsteht bei unvollständiger Verbrennung organischer Substanzen wie Auspuffgasen, Klimaanlagen, Gasthermen, Griller, Offene Feuer

## Symptome

| Anteil | Symptomatik |
| --- | --- |
| `10-20% CO-Hb` | **Kopfschmerzen**, Übelkeit, Schwindel, Abgeschlagenheit |
| `20-40% CO-Hb` | Schwindel, Mattigkeit, Willenlosigkeit,  Fehleinschätzungen |
| `40-50% CO-Hb` | Rausch, Unruhe, Tobsuchtsanfälle, Tod (sofort oder erst nach Wochen durch Organhypoxie) |

+ Kopfschmerzen
+ Bewusstseinsstörungen bis Bewusstlosigkeit, neurologische Ausfälle
+ Tachykardie, evt Arrhythmie, Blutdruckabfall
+ **keine typische Zyanose**
+ Übelkeit
+ Schwindel
+ Steigerung der Atemfrequenz
+ Sehstörungen

## Komplikationen

+ Bewusstlosigkeit
+ Krämpfe
+ Atem-Kreislauf-Stillstand

## Maßnahmen

+ Eigenschutz
+ Luftzufuhr
+ Rettung aus Gefahrenbereich (Eigenschutz)
+ Maximale Sauerstoffgabe
+ Absaugbereitschaft

## zu beachten

+ Atemschutzmasken sind wirkungslos
+ Vergiftung schon bei geringer Konzentration
+ S<sub>p</sub>O&#8322; nicht aussagekräftig
+ in geschlossenen Räumen Explosionsgefahr
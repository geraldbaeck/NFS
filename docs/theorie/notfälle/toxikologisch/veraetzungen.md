title:          Verätzungen
version:        0.0.5
authors:        gerald@baeck.at
ankikat:        Notfälle toxikologisch
                Notfallmedizin
md5:            d1cb46488f6708c23cb2edd14c150dbe
date_modified:  2019/06/04
date:           2019/05/10

## Definition

+ Verletzungen von Haut o. Schleimhaut durch chemische Stoffe 
+ In der Regel durch starke Säure oder Laugen
+ Grad der Schädigung ist abhängig von
    + Art
    + Konzentration
    + Menge
    + Einwirkdauer

## Symptome

| Aufnahmeweg | Symptome | Maßnahmen |
| --- | --- | --- |
| Augen | Rötung, Tränenfluss, Lichtscheu, Sehstörung | spülen (10min), beide Augen verbinden |
| Haut | Schorfbildung, glasige Verquellung | spülen , locker verbinden |
| Inhalation | Atemnot, Reizhusten, Schock, Lungenödem | O&#8322; |
| Ingestion | Abdominalschmerz, GI-Blutung | evt. Volumen |

## Charakteristika

+ Säuren => Koagulationsnekrose => Ätzschorf
+ Laugen => Kolliquationsnekrose => gallertartige Aufweichung

## Versorgungsablauf

+ Eigenschutz
    + Kontaminationsschutz
    + Hygienepaket
    + keine Mund zu Mund Beatmung
+ betroffene Stelle lange m. viel Wasser spülen
+ Pulver bürsten
+ Vitalfunktionen sichern
+ Monitoring, O&#8322;-Gabe
+ Wärmeerhalt
+ Bodycheck
+ Dokumentation

## Komplikationen

+ Schwere, tief gehende Wunden mit Infektionsgefahr
+ bei großflächiger Einwirkung Schockgefahr
+ bei Einatmung Lungenödem oft erst nach Stunden oder Tage
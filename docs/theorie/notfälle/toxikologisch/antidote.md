title:          Antidote
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle toxikologisch
                Notfallmedizin
md5:            ba1e5112e8af3fcbf1eb321381def45d
date_modified:  2019/06/02
date:           2019/06/02

| Gift | Antidot |
| --- | --- |
| Opiate | Naloxon |
| Benzos | Anexate |
| Insulin |  Glucose |
| Reizgas |  Pulmicort |
| CO | O&#8322; |
| Kokain | Dexmedetomidin |
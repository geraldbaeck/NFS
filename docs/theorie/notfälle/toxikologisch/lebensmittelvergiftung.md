title:          Lebensmittelvergiftung
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle toxikologisch
md5:            8adce1536d6d08ad50c116b314885b91
date_modified:  2019/01/07

## Definition
Durch Bakterien kontaminierte Nahrungsmittel und durch Bakterienstoffwechselprodukte. Besonders wenn mehere Personen nach dem Essen der gleichen Mahlzeit erkranken.

## Symptome
+ Übelkeit, Erbrechen
+ Durchfall
+ Fieber
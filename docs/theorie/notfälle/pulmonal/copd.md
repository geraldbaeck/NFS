title:          COPD
desc:           Chronic Obstructive Pulmonary Disease
date:           2019/01/06
version:        0.0.6
authors:        gerald@baeck.at
ankikat:        Notfälle Pulmonal
                Notfallmedizin
md5:            80c4b68ca6012e238e472fd308a2cf60
date_modified:  2019/04/20

## Definition

Chronic Obstructive Pulmonary Disease ist eine chronische Lungenerkrankung, mit einer **fortschreitenden Atemwegsobstruktion**.

COPD wird in GOLD-Stadien eingeteilt. Wichtigstes Kriterium für die Einteilung der COPD-GOLD-Stadien sind zwei Werte der Lungenfunktion: der FEV1-Wert (Einsekundenkapazität, engl. Forced Expiratory Pressure in 1 Second) und die forcierte Vitalkapazität (FVC). Diese Werte lassen sich durch einen Lungenfunktionstest bestimmen.

| COPD-Stadium | FEV1 (Sollwert = 100%) | FEV1/FVC |
| --- | --- | --- |
| `I (leicht)` | ≥ 80% Soll | < 70% |
| `II (mittel)` | 50% - 80% Soll | < 70% |
| `III (schwer)` | 30% Soll - 50% Soll | < 70% |
| `IV (sehr schwer)` | ≤ 30% Soll oder <br/>< 50% Soll plus chronische respiratorische Insuffizienz | < 70% |

## Ursachen

COPD entwickelt sich durch das jahrelange Einatmen schädlicher Stoffe und fasst zwei Krankheitsbilder zusammen:

## Chronische Bronchitis

Lang andauernde Entzündung der Bronchialschleimhaut

## Lungenemphysem

Irreversible "Überblähung" der Lunge in den Endaufzweigungen des Bronchialsystems

## Symptome

+ Husten am Morgen
+ Auswurf von zähem, glasigem Schleim
+ Ateminssuffizienz, Atemnot
+ verlängerte Ausatmung
+ evt Pfeifen beim Ausatmen
+ S<sub>p</sub>O&#8322; niedrig

## Komplikationen

+ Ateminssuffizienz
+ Rechtsherzinsuffizienz = massiv gestaute Halsvenen, Beinödeme
+ COPD Exacerbation: Akute Atemnot verursacht durch einen Infekt zb Bronchitis
+ Spontanpneumothorax

## Maßnahmen

+ keine körperliche Anstrengung
+ maximal erhöhter Oberkörper, mit Armen abstützen
+ Kleidung öffnen
+ Patient zu ruhiger Atmung anleiten (Lippenbremse)
+ Vorsicht bei der Sauerstoffgabe, zu viel Sauerstoff macht atemdepressiv

## Anamnese

Krankheitsgeschichte besonders wichtig, um Asthma auszuschließen

## Differentialdiagnosen

[Asthma und COPD](asthma-vs-copd.md) sind ähnliche Krankheitsbilder, allerdings entwickelt sich COPD erst im höheren Alter, Asthma beginnt meist in der Kindheit. Im Notfall ist eine Unterscheidung nur durch die Krankengeschichte möglich.
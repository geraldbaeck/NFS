title:          Atemstörungen
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Pulmonal
                Notfallmedizin
md5:            6ca68fa8a4d545c1a7d774170dba772b
date_modified:  2019/04/21
date:           2019/04/20

## Stridor/Spastik

+ = Krankhafte Atemgeräusche durch Verengung der Luftwege (Zischen, Pfeifen)
+ Inspiratorischer Stridor
    + = Atemgeräusch beim Einatmen
    + typisch für Verengung oder Verlegung der oberen Atemwege (Kehlkopf, Luftröhre, Hauptbronchien).
    + Ursachen: Epiglottitis, Krupp, Pseudokrupp, Schilddrüsenvergrößerung Fremdkörperaspiration
+ Exspiratorischer Stridor
    + Atemgeräusch beim Ausatmen
    + typisch für obstruktive Lungenerkrankungen
    + z. B. Asthma bronchiale, COPD oder chronische Bronchitis

## Bradypnoe

+ = pathologisch verlangsamte Atmung (4-8 Atemzüge pro Minute)
+ die bei Beeinträchtigungen des Atemzentrums vorkommt, z. B. bei Opiat-Vergiftungen

## Tachypnoe

+ gesteigerte Atemfrequenz wegen erhöhtem Sauerstoffbedarf bei
    + körperlicher Belastung, Fieber
    + Störungen des Säure- Basenhaushalts, erniedrigtem Sauerstoffangebot
    + verminderter Sauerstoffaufnahme aus der Atemluft (z. B. bei Lungenerkrankungen)
    + Störungen der Atemregulation (psychischer Erregung, Hyperventilation)

## Dyspnoe

+ unangenehm empfundene, erschwerte Atemtätigkeit
+ Ursachen, Wahrnehmung und Folgen dieses Symptoms können sehr unterschiedlich sein
+ zB Belastungsdyspnoe, Sprechdyspnoe, Ruhedyspnoe

## Spezielle Atemformen

+ Kussmaul-Atmung => regelmäßige tiefe Atmung bei Übersäuerung um CO&#8322; abzuatmen
+ Cheyne-Stokes => unregelmäßige Atemzüge, wird periodisch tiefer und wieder flacher
+ Biot ́sche Atmung => Atempause bei Schädigung im Gehirn
+ Schnappatmung => massive Atemstörung, keine suffiziente Atmung mehr
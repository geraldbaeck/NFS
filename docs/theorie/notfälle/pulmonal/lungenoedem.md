title:          Lungenödem
desc:           Pneumonie
date:           2019/01/06
version:        0.0.5
authors:        gerald@baeck.at
ankikat:        Notfälle Pulmonal
                Notfallmedizin
                HeavyRotation
md5:            fa7d68998385afde0db99b30ba85f8d6
date_modified:  2019/06/16

## Definition

+ Übertritt von Flüssigkeit aus den Kapillaren in die Alveolen
+ durch Mischung mit Atemluft bildet sich bläschenreicher Schaum
+ dieser Schaum behindert den Gasaustausch behindert und führt zu Atemnot

## Kardiales Lungenödem (am häufigsten)

+ Linksherzinsuffizienz mit Druckanstieg im Lungenkreislauf
+ Durch die Lungenstauung kommt es zum Übertritt von Flüssigkeit in den Zwischenzellraum und den Alveolarraum

## Toxisch allergisches Lungenödem

+ toxische Stoffe (zb Reizgas, Rauchgas) begünstigen eine höhere Durchlässigkeit der Lungenkapillarmembranen und damit den Übertritt von Flüssigkeit.

## Andere Ursachen

+ Acute Respiratory Distress Syndrome (ARDS) = Akutes Lungenversagen
+ Aufenthalt in großer Höhe
+ akute Überwässerung zb  durch eine Dialyse

## Symptome

+ Akute Atemnot (Dyspnoe bis Orthopnoe)
+ Atmung oberflächlich und schnell
+ RR hoch oder niedrig
+ spO&#8322; unter 90%
+ Rasselnde, brodelnde Atemgeräusche
+ Schaumiger Auswurf
+ evt. Zyanose
+ Reizhusten beim toxischen Ödem
+ Tachykardie

## Komplikationen

+ Atem-Kreislauf-Stillstand

## Maßnahmen

+ Keine körperlichen Anstrengung
+ erhöhter Oberkörper und tiefe Beine (Reduktion der Vorlast)
+ Kleidung öffnen
+ zu ruhigem Atem anleiten
+ Maximale O&#8322;-Gabe
+ EKG
+ NEF
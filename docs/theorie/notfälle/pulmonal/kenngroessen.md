title:          Kenngrößen Atmung
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Anatomie
                Notfallmedizin
md5:            90d3de65177e385927bf2dbbf587cf7e
date_modified:  2019/06/02
date:           2019/06/02

| Bezeichnung | Kürzel | Definition | Einheit |
| --- | --- | --- | --- |
| Atemfrequenz | AF | Atemzüge / min (12-15) | - |
| Atemzugsvolumen | AZV | Körpergewicht x7 | ml |
| Atemminutenvolumen | AMV | AF x AZV | ml |
| Totraum | - | KG x2 | ml |
| Vitalkapazität | - | Voll ausatmen und voll einatmen (ca. 4L) | - |
| Residialvolumen | - | Luftmenge, die nach max. Expiration in der Lunge verbleibt (1-1,5L) | - |
| Totalkapazität | - | Vitalkapazität + Residialvolumen | - |

![Lungenvolumen](https://rawgit.com/geraldbaeck/RS_WRK/master/charts/Lungenvolumen.svg)
Kennzahlen zu Lungenvolumen und Atmung[^1]
{: .imagewithcaption}

[^1]: [Gerald Bäck](mailto:gerald@baeck.at) with a [CC0-1.0](https://creativecommons.org/licenses/by-sa/2.0/at/deed.de) public domain dedication = no copyright
title:          Lungenembolie
desc:           Pulmonalembolie
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Notfälle Pulmonal
                Notfallmedizin
md5:            96ed6c9f82c024db71515d375bc7bf29
date_modified:  2019/04/21

## Definition

Einschwemmen von Blutgerinnseln (Thromben) aus tiefen Bein- bzw. Beckenvenen in Lunge führt Verschluss von Lungenarterien und damit zum Lungeninfarkt.

Besonders gefährdet: ältere Personen, Bettlägrige, nach Langstreckenflügen, übergewichtige Raucherinnen, die die Pille nehmen

## Ursachen

+ Verschleppung von Thromben aus Bein-/Beckenvenen
+ Fettembolien (selten): frei gesetzte Gewebsfette (zb dr Knochenbrüche mit Knochenmarksbeteiligung, nach großen chirurgischen Eingriffen)
+ Fruchtwasserembolie: Fruchtwasser gelangt in den venösen Kreislauf und führt zur Thrombenbildung
+ Luftembolie: nach eindringen von Luft in den venösen Kreislauf (zb Suizid)

## Symptome

+ Atemnot
+ atemabhängige Brustschmerzen
+ Husten ev. Bluthusten
+ Tachykardie
+ evt initialer Kollaps, Synkopen (15%)

## Komplikationen

+ Bewusstlosigkeit
+ Atem-Kreislauf-Stillstand

## Maßnahmen

+ Keine Anstrengung, keine aktive Bewegung
+ erhöhter Oberkörper und tiefe Beine
+ Kleidung öffnen
+ zu ruhigem Atem anleiten
+ Maximale O&#8322;-Gabe

## Differentialdiagnosen

Wegen der sehr unspezifischen Symptomatik wird nur jede 3.-5. Lungenembolie erkannt. Bei ausgeprägter Zyanose muss trotz 100% O&#8322; Gabe immer von einem Verdacht auf Lungenembolie ausgegangen werden.
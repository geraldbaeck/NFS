title:          Lungenentzündung
desc:           Pneumonie
date:           2018/12/27
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Notfälle Pulmonal
                Notfallmedizin
md5:            b6899be58c8087dd038b9867becdce36
date_modified:  2019/04/20

## Definition

+ Entzündung des Alveolarraums und/oder des interstitiellen Lungengewebes
+ häufigste tödliche Infektionserkrankung in Industrienationen
+ wird präklinisch oft übersehen

## Ursachen

+ Erregerspektrum unterscheidet sich nach Altersgruppe und Infektionsursache (ambulant oder im Krankenhaus erworben)
+ Am häufigsten: **Pneumokokken**
+ Inkubationszeit: Stunden bis Tage

## Risikofaktoren

+ Grunderkrankungen der Lunge, z. B. COPD, Mukoviszidose oder Lungenemphysem
+ allgemeine Abwehrschwäche, z. B. bei HIV-Infektion, Chemotherapie, Immunsuppression, Krebs, Diabetes Mellitus oder Alkoholismus
+ Allergien
+ hohes Alter
+ kleine Kinder
+ vorbestehende Influenza

## Symptome

+ Hohes Fieber, Schüttelfrost
+ Husten mit eitrigem Auswurf (Sputum)
+ Tachypnoe und Dyspnoe
+ Brustschmerz
+ Schweißausbruch
+ Bewusstseinstrübung
+ Schwere Störung des Allgemeinbefindens

## Komplikationen

+ eingeschränkte Atmung (respiratorische Insuffizienz)
+ Blutvergiftung (Sepsis)
+ Pleuraerguss (Flüssigkeit zw. Lunge & Rippen)

## Maßnahmen

+ erhöhter Oberkörper
+ S<sub>p</sub>O&#8322;-Gabe 10-15 Liter
+ allg. Maßnahmen
+ Mundschutz, Einmalhandschuhe Wischdesinfektion
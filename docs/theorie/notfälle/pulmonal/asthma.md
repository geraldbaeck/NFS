title:          Asthma
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Notfälle Pulmonal
                Notfallmedizin
md5:            526a0715abed967b7c331da9c8feafe8
date_modified:  2019/05/16

## Definition

+ Krampf der kleinen Bronchien (Bronchospasmus)
+ Schwellung der Bronchialschleimhaut
+ zähe Schleimbildung (Hypersekretion)
+ Hauptproblem bei Ausatmung (pfeifendes Geräusch).
+ **Status asthmaticus** =lang anhaltende oder sich in kurzer Zeit wiederholende Anfälle

## Ursachen

+ allergische Reaktion
+ Stress
+ körperliche Anstrengung
+ Infektion
+ medikamentös indiziert (zb durch Aspirin)

## Symptome

+ plötzlich auftretende hochgradige Atemnot
+ Schwierigkeiten beim Ausatmen => überblähung der Alveolen
+ Einsetzen der Atemhilfsmuskulatur
+ Pfeifende Atemgeräusche (Stridor)
+ Tachykardie

## Komplikationen

+ Status asthmaticus (lang andauernd oder kurzzeitig wiederholt)
+ Anaphylaxie
+ Atem-Kreislauf-Stillstand

## Maßnahmen

+ Keine körperliche Anstrengung
+ Lagerung max. erhöhter Oberkörper
+ mit Armen abstützen lassen
+ beengende Kleidungsstücke öffnen
+ beruhigen
+ Patient gemeinsam zu atmen anleiten (Lippenbremse)
+ S<sub>p</sub>O&#8322; < 92% lebensbedrohlich
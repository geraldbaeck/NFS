title:          Akutes Koronarsyndrom
desc:           Angina Pectoris und Herzinfarkt
date:           2019/01/06
version:        0.0.5
authors:        gerald@baeck.at
ankikat:        Notfälle Kardiovaskulär
                Notfallmedizin
md5:            1ed0dbb4bffb7cbcd6182a471760df5b
date_modified:  2019/04/28

## Definition

+ = Missverhältnis zwischen Sauerstoffbedarf und -angebot des Herzmuskels
+ Überbegriff akutes Koronarsyndrom: Angina Pectoris und Herzinfarkt
+ Leitsymptom: Brustschmerz

## Angina Pectoris

+ drückender, beengender Brutschmerz
+ verengte Herzkranzgefäße verursachen eine Minderversorgung des Herzmuskels mit Sauerstoff
+ Auslöser:
    + körperliche oder seelische Belastung
    + Kälte
    + Nikotinmissbrauch

Wenn Symptome sehr intensiv sind und vom Arzt verabreichte Medikamente nicht wirken kann ein Herzinfarkt vorliegen.

## Herzinfarkt

+ Ursache: komplett verschlossenes Herzkranzgefäß
+ => Absterben von Herzmuskelgewebe = Unterschied zur Angina Pectoris
+ Häufigste lebensbedrohliche Komplikation wie zb Kammerflimmern.

## Ursachen

+ Hoher Blutdruck führt zu Schäden in der Gefäßinnenhaut
+ => vermehrte Plaquebildung (arteriosklerotische Plaques)
+ => Kalkeinlagerung/Verhärtung (Arteriosklerose)
+ => Gefäßverengung (Stenose)

## Risikofaktoren

+ Fettstoffwechselstörungen
+ Bluthochdruck
+ Rauchen
+ Diabetes mellitus
+ Übergewicht
+ Bewegungsmangel
+ Stress

## Symptome

+ Leitsymptom Schmerz:
    + Brustbein 90% (atemunabhängig)
    + linke Schulter 55%
    + linke Brust 50%
    + linker Arm 50%
    + rechte Schulter 25%
    + Unterkiefer 20%
    + Bauchraum 15%
    + CAVE: Diabetiker haben eingeschränkte Schmerzwahrnehmung
+ Engegefühl in Brust
+ Ausstrahlung in Arme, Hals, Kiefer, Oberbauch & Rücken
+ Kaltschweißigkeit
+ Blässe
+ Übelkeit
+ Angst- und Vernichtungsgefühl
+ Stuhl- und Harndrang
+ Keine Besserung durch Nitro- Spray

Symptome treten nicht immer gleich oder gemeinsam auf. Außerdem gibt es sog. stumme Infarkte.

## Komplikationen

+ Rhythmusstörungen
+ Vorwärts- und Rückwärtsversagen
+ Herz- Kreislauf- Stillstand

## Maßnahmen

+ Keine Anstrengung
+ erhöhter Oberkörper
+ Kleidung öffnen
+ Pat. beruhigen + zum ruhigen Atmen bringen
+ O&#8322; unter 95%
+ Defi Pads kleben
+ NEF
+ EKG

## Anamnese

+ genauer Beginn
+ Entwicklung der Symptomatik
+ Vorerkranken abklären (insbesondere TIA, Schlaganfall, größere Verletzungen oder OPs innerhalb der letzten 3 Wochen)
+ Medikamente (Blutverdünner!!!)
title:          Herzversagen
date:           2019/01/06
version:        0.0.5
authors:        gerald@baeck.at
ankikat:        Notfälle Kardiovaskulär
                Notfallmedizin
md5:            e83e242dfe1531d2fad711db279416ef
date_modified:  2019/05/08

## Definition

Leistungseinschränkung des Herzens mit Verminderung der Pumpleistung bei ausreichendem Blutangebot. Ausgelöst durch Herzmuskelschwäche.

| Insuffizienz | Vorwärtsversagen | Rückwärtsversagen | chronisch | akut |
| --- | --- | --- | --- | --- |
| Rechts | Ausfall der Pumpfunktion: <ul><li>ungenügende Durchblutung der Lunge</li><li>kein Gasaustausch</li><li>Zyanose</li><ul> | Rückstau in den Körper: <ul><li>Halsvenen</li><li>Leberstauung</li><li>Aszites</li><li>Beinödeme</li></ul> | <ul><li>Stauungsorgane</li><li>Stauungsdermatitis</li><li>Ulcus Cruris</li></ul> | <ul><li>= Folge der Lungenembolie</li></ul>  |
| Links | Ausfall der Pumpfunktion: <ul><li>ungenügende Durchblutung des Körpers</li><li>=> kardiogener Schock</li></ul> | <ul><li>Rückstau in die Lunge</li><li>Lungenödem</li></ul> | <ul><li>Abnehmende Belastbarkeit</li><li>Zunehmende Atemnot</li><li>Zunehmende Lungenstauung</li></ul> | <ul><li>Dekompensation</li><li>Lungenödem</li><li>Schock</li></ul> |

### Symptome

+ Atemnot (Dyspnoe)
+ Tachykardie (Herzrasen)
+ Blutdruckabfall (Hypotonie)
+ Zyanose

### Maßnahmen

+ keine körperliche Anstrengung
+ erhöhter Oberkörper und Tieflagerung der Beine
+ Kleidung öffnen, ruhiges Atmen
+ Tieflagerung der Beine
title:          Blut im Harn
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Kardiovaskulär
md5:            0c60f76dc7d74e1e524057640a95cc3f
date_modified:  2019/01/07

## Ursachen
+ evt Nierenkolik
+ Blasentumor
+ Gerinnungshemmer
+ selten lebensbedrohlich

## Maßnahmen
+ Allgemeine Maßnahmen
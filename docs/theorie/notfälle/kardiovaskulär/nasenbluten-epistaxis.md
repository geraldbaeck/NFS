title:          Nasenbluten
desc:           Epistaxis
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Notfälle Kardiovaskulär
                Notfallmedizin
md5:            339346e630927f30b0a837f40a1a1e24
date_modified:  2019/05/06

## Ursachen

+ Hypertonie
+ mechanische Reizung
    + Trauma
    + Nasenbohren, -putzen
+ rasche Überwindung von Höhenunterschieden
+ gerinnungsbeeinflussende Medikamente, Gerinnungsstörungen
+ vermehrte Durchblutung durch Erkältung, Heuschnupfen

## Komplikationen

+ Aspiration von Blut
+ Übelkeit, Erbrechen

## Maßnahmen

+ aufrecht hinsetzen
+ Oberkörper und Kopf nach vorne
+ Blutendes Nasenloch zudrücken
+ kalte Kompresse in den Nacken
+ Nierentassen und Zellstoff reichen
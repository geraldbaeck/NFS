title:          Verletzungen durch Kathedersysteme
desc:           Epistaxis
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Kardiovaskulär
md5:            08828f063ce683b383df36de01716704
date_modified:  2019/01/07

## Ursachen  
+ uabsichtliches Herausreißen eines Harnkatheders

## Symptome  
+ große Schmerzen
+ Blutiger Harnkatheders
+ Wunden im Bereich des Harnleiters

## Maßnahmen  
+ Harnfluss eindämmen (saugendes Material, Lagerung)
+ evt Wundversorgung
+ Lagerung nach Fritsch
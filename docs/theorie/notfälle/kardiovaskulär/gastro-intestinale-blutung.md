title:          Gastro-Intestinale Blutung
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle Kardiovaskulär
                Notfallmedizin
md5:            5f04549def2a182a63240c2423682618
date_modified:  2019/04/21
date:           2019/04/21

## Definition

+ = akuter oder chronischer Blutverlust in das Lumen des Verdauungstraktes
+ Obere gastrointestinale Blutung: Ursprung oberhalb des Zwölffingerdarms
+ Untere gastrointestinale Blutung: unterhalb des Zwölffingerdarms

## Symptome

+ Hämatemesis (Bluterbrechen)
+ Melaena (Blutstuhl/Teestuhl)
+ Hämatochezie (Rektalblutung)

## Ursachen

der oberen Gastrointestinalen Blutung (nach Häufigkeit)

+ Magengeschwür oder Zwölffingerdarmgeschwür
+ Ösophagusvarizen
+ Mallory-Weiss-Syndrom (Schleimhauteinrisse zw. Magen u. Ösophagus)
+ Boerhaave-Syndrom (spontane Ösophagus Perforation)
+ Magenkarzinom oder Ösophaguskarzinom

der unteren Gastrointestinalen Blutung

+ Polypen und Tumore des Darmes, z.B. Dickdarmkrebs
+ Chronisch-entzündliche Darmerkrankungen wie (Morbus Crohn und Colitis ulcerosa)
+ Divertikulitis (Entzündung der Darmschleimhaut)
+ Darmentzündungen durch Erreger, Parasiten, Autoimmungeschehen
+ Hämorrhoiden, Fissuren oder Fisteln des Anus
+ Gefäßfehlbildungen
+ Mesenterialinfarkte des Magen-Darm-Traktes
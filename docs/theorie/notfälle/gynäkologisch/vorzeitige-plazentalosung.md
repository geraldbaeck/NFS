title:          Vorzeitige Plazentalösung
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
                Notfallmedizin
md5:            7dad84af2b55db0c848e6966e0ef3c06
date_modified:  2019/04/01

## Definition

Durch die Bildung eines Hämatoms hinter der Plazenta kann es zu einer Lösung kommen. 

## Ursachen

+ Gewalteinwirkung / Trauma
+ Erkrankungen (Eklampsie)
+ Drogenmissbrauch
+ Hypertonie
+ Nikotin

Dadurch wird die Versorgung des Kindes eingeschränkt bzw unterbunden.

## Symptome

+ Plötzlich auftretende Schmerzen
+ Gespannte Bauchdecke
+ dunkelrote, vaginale Schmierblutung (Peripartale Blutung) unterschiedlich stark

## Komplikationen

Mutter:

+ starke Blutung => Schockzeichen bei der Mutter
+ Unruhe, Schwäche, Angst- und Durstgefühl und Übelkeit

Kind:

+ akuter Sauerstoffmangel durch Mangelversorgung

## Maßnahmen

+ Lagerung nach Fritsch
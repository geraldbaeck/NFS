title:          Nabelschnurvorfall
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
md5:            9b5bfa35ede038bed040fd072b151a96
date_modified:  2019/04/01

## Definition

+ Nach dem Blasensprung kann sich die Nabelschnur um das Kind wickeln.
+ Während der Geburt wird die Nabelschnur im Geburtskanal abgeklemmt
+ => Kind unterversorgt.

+ Prädisposition bei:
    + Mehrlingsschwangerschaft
    + Frühgeburt
    + Beckenendlage/Querlage
    + Polyhydramnion (=pathologische Vermehrung der Fruchtwassermenge)

## Symptome

+ Blasensprung
+ Abgang von Blut, Schleim, Fruchtwasser
+ Aus der Scheide hängende Nabelschnur

## Komplikationen

+ Lebensgefahr für das Kind

## Maßnahmen

+ Becken Hochlagern
+ Kopf des Kindes mit beiden Fingern reindrücken => Finger "verschwinden" in der Vagina
+ Auf Pulsation der Nabelschnur achten
+ Nächstes KH mit Aviso
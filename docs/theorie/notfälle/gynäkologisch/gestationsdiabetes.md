title:          Gestationsdiabetes
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
                Notfallmedizin
md5:            16a21f8df8014049dedc3cf669c39c59
date_modified:  2019/05/10
date:           2019/05/09

## Definition

+ = Diabetes der Mutter, der erstmals in der Schwangerschaft diagnostiziert wird
+ Meistens Normalisierung nach der Geburt
+ auch: Schwangerschaftsdiabetes, Gestationsdiabetes mellitus (GDM) oder Typ-4-Diabetes

## Risikofaktoren

+ Übergewicht
+ > 30a
+ erbliche Vorbelastung mit Diabetes Mellitus

## Komplikationen

+ Hypertonie
+ Präeklampsie
+ anfälliger für Harnwegsinfektionen und Scheidenentzündungen
+ erhöhte Kaiserschnittrate

## Maßnahmen / Vorbeugung

+ Screening mit Mutter-Kind-Pass!
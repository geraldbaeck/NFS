title:          Phasen der Geburt
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
                Notfallmedizin
                HeavyRotation
md5:            815386564c7ae8d9bd2433e61447e2f0
date_modified:  2019/05/10

| Phase | Dauer | Wehenintervall | Wehendauer |
| --- | --- | --- | --- |
| `Beginn` | - | 2-3W/10min | 30s |
| `Eröffnungsperiode` | 7-12h | 3-5min | 30-60s |
| `Austreibungsperiode` | 0.5-3h | 2-3min | 60-90s |
| `Nachgeburtsperiode` | 15-20min | - | - |

## Eröffnungsperiode

+ 7-12 Std. Dauer
+ regelmäßige Wehen alle 3-5 min
+ Wehendauer ca. 30-60 Sek.
+ Kind wird durch Wehen zum Becken Richtung Geburtskanal gepresst
+ Muttermund verkürzt sich und wird zu einer flachen, runden Öffnung
+ gegen Ende oft Blasensprung
+ Muttermund komplett offen
+ Maßnahme: Transport

## Austreibungsperiode

+ 1,5-3 Std. Wehen alle 2-3 min. (Dauer 60-90 Sek.)
+ Kontraktion Gebärmutter und Bauchdecke = Presswehen
+ Drehbewegungen von Kopf und Körper, Erscheinen des Kopfes, weicht in der Wehenpause wieder zurück
+ Das Hinterhaupt stemmt sich gegen die Schambeinfuge, von hinten erscheint das Gesicht, Kopf dreht sich zur Seite, erscheinen der vorderen Schulter dann der hinteren
+ zuletzt gleitet der übrige Körper rasch raus, dann restliches Fruchtwasser
+ Maßnahmen:
    + Geburt kein Transport
    + Dammschutz
    + Geburtszeitpunkt notieren

## Nachgeburtsperiode

+ ca. 15-20 min.
+ durch Nachgeburtswehen wird die Plazenta von der Gebärmutter gelöst und nach außen gepresst.
+ Blutungen in Nachgeburtsperiode können aufgrund einer unvollständigen Plazentalösung, Muskelschwäche der Gebärmutter oder Gerinnungsstörung auftreten => Maßnahme: Wehe anreiben
+ Plazenta aufbewahren

## Versorgung der Kindes

+ Schutz vor Wärmeverlust
+ Abtrocknen und in Decke/Handtuch einwickeln. (auch Kopf)
+ Taktile Stimulation → Rücken / Thorax, Fußsohlen
+ Beurteilung der Vitalparameter
    1. Atmung
    2. Muskeltonus
    3. Stamm → rosig?
+ Abnabeln
+ Dokumentation Mutter-Kind-Pass

## Versorgung der Mutter

+ Überprüfung der Vitalparameter
+ Lagerung – ggf. Nachgeburt mitnehmen
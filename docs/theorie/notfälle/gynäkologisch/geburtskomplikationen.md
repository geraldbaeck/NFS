title:          Fehlgeburt, Totgeburt, frühgeborene Lebendgeburt
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
md5:            dc3a4caac6e0fdffc63619b33fc70f7f
date_modified:  2019/01/07

| Bezeichnung | SSW | Gewicht | Lebenszeichen |
| --- | --- | --- | --- |
| `[Fehlgeburt (Abortus)](abortus.md)` | - | < 500g | tot geboren |
| `Totgeburt` | - | > 500g | tot geboren |
| `[frühgeborene Lebendgeburt (Frühgeburt)](fruehgeburt.md)` | < 37 SSW | > 500g | Nach Scheidung vom Mutterleib:<ul><li>Herzschlag oder</li><li>Nableschnurpulsation oder</li><li>Lungenatmung oder</li><li>willkürliche Muskelbewegung (nur Ö)</li></ul> |
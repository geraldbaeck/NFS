title:          Eileiterschwangerschaft
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
                Notfallmedizin
md5:            8058d790411dd1a911dae27f98888138
date_modified:  2019/05/10

## Definition

+ Befruchtete Eizelle nistet sich im Eileiter oder in der Bauchhöhle ein
+ Durch das Wachstum des Follikels kommt es zu einer Zerreißung des Eileiters
+ => zu lebensbedrohliche Blutungen

## Symptome

+ einseitige Unterbauchschmerzen meist 5-6. SSW
+ Akutes Abdomen nach intensiven Schmerz
+ evtl. Vaginale (Schmier-) Blutung

## Komplikationen

+ starke Blutung
+ Eileiterruptur

## Maßnahmen

+ Lagerung nach Fritsch
+ Voranmeldung im KH
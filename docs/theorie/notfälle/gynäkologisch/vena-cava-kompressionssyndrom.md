title:          Vena-Cava-Kompressionssyndrom
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
                Notfallmedizin
md5:            94894395771485693b447ceaafea92bb
date_modified:  2019/05/10

## Definition

+ Gebärmutter drückt in Rückenlage auf die untere Hohlvene
+ => behinderter Blutrückfluss
+ => Kollaps

## Symptome

+ Kollaps
+ Schwindel
+ Übelkeit

## Komplikationen

+ Bewusstlosigkeit

## Maßnahmen

+ Linksseitenlage
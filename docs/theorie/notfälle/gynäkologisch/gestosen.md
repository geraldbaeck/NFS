title:          Gestosen
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
                Notfallmedizin
md5:            973bd792b5be6757456e1b6070bf0835
date_modified:  2019/06/07
date:           2019/06/02

+ Frühgestosen (1. Drittel)
    + [Hyperemsis gravidarum](hyperemesis-gravidarum)
    + Ptyalismus gravidarum (vermehrter Speichelfluss, extrem selten)
+ Spätgestosen (ab SSW 20)
    + **S**chwangerschafts **i**nduzierte **H**ypertonus (SiH)
    + Präeklampsie
    + HELLP Syndrom
    + Eklampsie
    + Komplikationen
        + Hypertonie
        + Proteinurie
        + Ödeme
        + Mikroangiopathie (=Erkrankung der kleinen Blutgefäße)
            + Lungenödem
            + temporäre Erblindung
            + Nierenversagen
            + Leberversagen
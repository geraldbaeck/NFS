title:          Hyperemesis gravidarum
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
                Notfallmedizin
md5:            b45a15bede5ff9240b30f2f68fe5310d
date_modified:  2019/05/27
date:           2019/05/09

## Definition

+ ist das übermäßige & anhaltende Erbrechen auch bei leerem Magen
+ oft über den ganzen Tag wie auch nächtliches Erbrechen
+ Tritt im ersten SS-Drittel auf und endet etwas nach der 14. SSW
+ vereinzelt auch bis zur Geburt anhaltende Übelkeit & Erbrechen

## Maßnahmen

+ Flüssigkeit iv
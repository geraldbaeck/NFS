title:          Suizid
date:           2019/01/06
version:        0.0.5
authors:        gerald@baeck.at
ankikat:        Notfälle Psychiatrisch
                Notfallmedizin
md5:            f84cc11c69b3f5852c16fec0328666ef
date_modified:  2019/05/01

## Definition

= vorsätzliche Beendigung des eigenen Lebens

+ keine eindeutigen Anzeichen
+ jede Form der Andeutung zur Selbsttötung ist ernst zu nehmen

| Begriff | Definition |
| --- | --- |
| Suizid | Selbsttötung |
| Suizidversuch | Versuch des Suizids ohne tödlichen Ausgang |
| Suizidale Geste | selbstschädigendes Verhalten, Versuch Aufmerksamkeit zu bekommen |
| Suizidgedanken | Vorstellung über suizidales Verhalten |

## Suizidalität

+ =Selbstmordgefährdung
+ Suizidalität ist krankheitswertig und verpflichtet zu therapeutischem Handeln
+ Jeder Mensch kann suizidale Phantasien und Phasen entwickeln.
+ Motive
    + Der Wunsch, tot zu sein (Suizidversuch).
    + Der Wunsch nach Hilfe (parasuizidale Geste).
    + Der Wunsch, Ruhe zu haben und von den aktuellen Belastungen nichts mehr mitzubekommen (Parasuizidale Pause).

## Präsuizidales Syndrom

+ Antriebssteigerung
+ Aussagen über Sinnlosigkeit des Lebens
+ unerklärte plötzliche Ruhe
+ Selbstverletzungen
+ Testament verfassen

## Risikofaktoren

Besonders gefährdet sind Menschen:

+ mit psychischen Störungen
+ mit Drogenabusus
+ sehr alte
+ chronisch, unheilbar Kranke
+ in Lebenskrisen
+ nach Lebensverändernden Ereignissen (zb Flucht, Unfall, Scheidung)
+ mit Suizidversuchen in der Vorgeschichte

## Maßnahmen

+ Kriseninterventionsteam
+ Beziehungsaufbau
+ Suizidgedanken direkt ansprechen
+ notfalls Polizei rufen => Unterbringung
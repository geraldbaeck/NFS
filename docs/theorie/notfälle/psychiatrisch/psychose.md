title:          Psychose
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle Psychiatrisch
                Notfallmedizin
md5:            2b7020e275743b3922892c27b694a3f4
date_modified:  2019/04/27
date:           2019/04/27

## Definition

= Überbegriff für verschiedene schwerwiegende psychische Störungen, bei denen Betroffene den Bezug zur Realität verlieren.

## Arten

+ Organische Psychose
+ Substanzinduzierte Psychose
+ Nichtorganische Psychose
    + Schizophrene Psychose
    + Affektive Psychose
        + Manie
        + Bipolare Störung
        + Schwere Depression

## Symptome

+ Störung der Realitätswahrnehmung
+ Ich-Funktion wird nicht erkannt
+ Sinn wird nicht erfasst
+ Wahnvorstellungen
+ Halluzinationen

## Maßnahmen

+ Respekt vor der erkrankten Person
+ Entschärfen der Situation
+ Beruhigen, Hilfe anbieten
+ Sofortige Alarmierung der Polizei bei „tobender Psychose“
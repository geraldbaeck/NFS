title:          Suchterkrankung
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Psychiatrisch
                Notfallmedizin
md5:            2b291de124aa331ac9dbebb57443684b
date_modified:  2019/05/08
date:           2019/04/27

## Definition

= Abhängigkeit von psychoaktiven Substanzen

## Komplikationen

+ Intoxikation
+ Entzug
+ Alkohol
    + motorische Unruhe
    + RR/HF-Anstieg
    + Schwitzen
    + Reizbarkeit
    + Gefahr: Entzugsepi
+ Benzodiazepine
    + Muskelzucken/-Krämpfe
    + Schwitzen
    + Schlafstörungen
    + Angst
    + Unruhe
    + Depression
    + Gefahr: Entzugsepi
+ Opiate
    + Verlangen nach Drogen
    + Schwitzen/Frieren
    + Tränenfluss
    + laufende Nase
    + Gänsehaut
    + Gliederschmerzen
    + RR/HF/Temp.-Anstieg
    + Erbrechen/Durchfall
    + Gefahr: Atemdepression
+ Kokain
    + Euphorie
    + Depressive Stimmung (bei Entzug oder beim Abklingen der Wirkung)
    + Reizbarkeit
    + Exsiccose
    + Gefahr: Herzrhythmusstörung

## Maßnahmen

+ Sicherung der Vitalfunktionen
+ Transport in eine geeignete Abteilung: Intern, ggf. Toxi/WSP
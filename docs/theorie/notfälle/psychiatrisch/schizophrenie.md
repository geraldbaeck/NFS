title:          Schizophrenie
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Psychiatrisch
md5:            cd8cb4b59d7919e66d9fa273590e4ea5
date_modified:  2019/01/07

## Definition  
Störung von 

+ Denken, 
+ Antrieb und 
+ Wahrnehmung, 
+ Affektstörungen

## Symptome  
+ Wahn
+ Halluzinationen
+ Unruhe
+ Anspannung
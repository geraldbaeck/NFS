title:          Angststörung, Panikattacken
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Notfälle Psychiatrisch
                Notfallmedizin
md5:            5548a4fd4cf1c4c2646d5274d3a400c5
date_modified:  2019/05/18

## Definition

+ Angst steht im Vordergrund
+ schränkt das unmittelbare Handeln und Leben ein
+ mögliche Auslöser:
    + gerichtete Ängste (Phobien)
    + ungerichtete Ängste (Panikstörung)

Im Rettungsdienst begegnen uns oft Patienten, die fürchten an einer lebensgefährlichen Erkrankung zu leiden.

## Symptome

+ Angstgefühl
+ Herzrasen, Herzklopfen, Herzstolpern
+ Mundtrockenheit, Unruhe, Zittern, Kribbeln
+ Schweißausbrüche (nicht Kaltschweißigkeit)
+ Hitzegefühl oder Kälteschauer
+ Engegefühl in Brust und Kehle, Erstickungsgefühl ggf Hyperventilation
+ Depersonalitätsgefühle („nicht mehr sich selbst sein“ oder „neben sich stehen“)
+ Derealisationsgefühle (= verfremdete Wahrnehmung der Umwelt)

CAVE: MCI ist oft von Todesangst begleitet
{: .warning}

## Komplikationen

+ Hyperventilationssyndrom => Rückatmen

## Maßnahmen

+ Genaue Anamnese
+ Ruhe ausstrahlen
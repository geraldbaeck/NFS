title:          Akutes Abdomen
date:           2019/01/06
version:        0.0.8
authors:        gerald@baeck.at
ankikat:        Notfälle
                Notfallmedizin
md5:            e809d5abbc68852d6c27614bf3c4abdc
date_modified:  2019/06/18

## Definition

+ Akute bedrohliche Erkrankung im Bauchraum
+ präklinisch oft nicht eindeutig eingrenzbar
+ Symptomentrias aus
    + starke, akute Bauchschmerzen
    + Abwehrspannung der Bauchdeckenmuskulatur (bretthart schon beim berühren)
    + mehr oder weniger stark ausgeprägte Kreislaufdekompensation bis zum Schock

## Ursachen

+ Organbedingt
    + Entzündungen
        + Bauchspeicheldrüse (Pankreatitis)
        + Bauchfell (Peritonitis)
        + Blinddarm (Appendizitis)
        + Magen (Ulkus)
        + Gallenblase (Cholezystitis)
        + Speiseröhre (Ösophagitis)
        + Dickdarm (Divertikulitis)
    + Perforationen
        + bei Magengeschwür
        + bei Zwölffingerdarmgeschwür
        + durch Verletzungen (Schuss, Stich, Koloskopie)
        + Tumore
        + Fremdkörper (oral, rektal)
    + Koliken (Galle, Niere, Darm)
    + Steine (Galle, Niere)
    + Darmverschlüsse (Illeus)
+ Gefäßbedingt
    + Blutungen im Magen-Darm-Trakt oder in Speiseröhre
    + Mesenterialinfarkt (Verschluss im Gekröse)
    + Aortenaneurysma
+ Gynäkologische Erkrankungen
+ Lebensmittelvergiftung (Gastroenteritis)
+ Meteorismus (verzwickter Schaß)
+ Abdominaltrauma (Bauchschlag)

## Symptome

+ Symptomentrias aus
    + starke, akute Bauchschmerzen
    + Abwehrspannung der Bauchdeckenmuskulatur
    + mehr oder weniger stark ausgeprägte Kreislaufdekompensation bis zum Schock
+ evt gekrümmte Haltung
+ Veränderungen der Darmtätigkeit
+ Störung der Darmentleerung
+ Koliken, Krämpfe
+ Übelkeit, Erbrechen
+ Fieber

## Maßnahmen

+ Leicht erhöhter Oberkörper
+ Knierolle oder auf Seite liegend mit angezogenen Beinen
+ Ess-, Trink- und Rauchverbot

## Physikalische Untersuchung bei abdominalen Beschwerden

+ Auskultation des Abdomens
    + 4 Quadranten abhören (am weitesten vom Schmerzpunkt entfernt beginnen)
    + Darmgeräusche beurteilen (normal / verstärkt / vermindert / fehlend / metallisch)
+ Inspektion
    + Bauchdecke gespannt / geschwollen
+ Tastbefund
    + (am weitesten vom Schmerzpunkt entfernt beginnen)
    + Bauchdecke: weich / hart?
    + Druckschmerz: lokal vs diffus

## Chirurgische Anamnese

+ **O**: Wo/Wann haben die Schmerzen begonnen?
+ **P**: Wurde etwas unternommen? Mit welchem Effekt?
+ **Q**: Qualität (brennen, schneidend, Krampfartig...)
+ **R**: Wo ist der Hauptschmerz? Strahlt der Schmerz aus?
+ **S**: Schmerzstärke
+ **T**: Hat sich im Verlauf etwas am Schmerz geändert? (Verbesserung, Verschlechterung, kein Effekt)

+ **S**: Begleitsymptomatik: Fieber? Erbrechen? Durchfall?
+ **A**: Allergien
+ **M**: Medikamente, wann zuletzt eingenommen? (ev Überdosis)
+ **P**: Frühere Erkrankungen / Operation (Narben) im Bauchraum, Schwangerschaft, Blinddarm-OP, bekannte Steinleiden?, bekannte Entzündungsleiden
+ **L**: Letzter Stuhl/Harn / wie hat der ausgesehen. Letzte Mahlzeit
+ **E**: Ereignis
+ **R**: Risikofaktoren, Diabetes, Alkohol, Adipositas
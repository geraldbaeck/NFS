title:          Allergie
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle allgemein
                Notfallmedizin
md5:            a3a739067cc58421f28b7eb16e2631ed
date_modified:  2019/04/21
date:           2019/04/20

## Definition

= Fehlreaktion des Immunsystems

+ unschädlicher Stoff wird als gefährlich eingestuft
+ überschießende Abwehrreaktion des Immunsystems ist die Folge. (große Mengen Histamin)
+ Erstkontakt mit der betreffenden Substanz führt zur Sensibilisierung
+ Jeder weitere Kontakt zu einer übersteigerten Immunreaktion
+ Bis zu 6 Stunden nach der Erstreaktion kann es zu einer Zweitreaktion kommen

Es gibt verschiedene Arten von allergischen Reaktionen. Die häufigste, und für den Rettungsdienst wichtigste ist die **ALLERGISCHE REAKTION VOM FRÜHTYP (Typ 1 Reaktion)**

## Auslöser

+ Medikamente (Schmerzmittel, Antibiotika, Kontrastmittel)
+ tierische Gifte (Bienen, Wespen, Schlangen, Spinnen)
+ Nahrungsmittel (Nüsse, Schalentiere, Fisch, Milch)

## Stadien der Typ 1 Reaktion

+ Stadium 1
    + Haut- u. Schleimhautreaktionen
        + Konjunktivitis (Bindehautentzündung)
        + Rhinitis (Heuschnupfen)
    + Juckreiz
        + Exanthem (Hautausschlag)
        + Urtikaria (Nesselsucht)
+ Stadium 2
    + leichter Bronchospasmus (Asthma)
    + Hypotonie
    + Tachykardie
    + Ev. Herzrhythmusstörungen
+ Stadium 3
    + Anaphylaktischer Schock
    + Verlegung der Atemwege (Bronchospasmus, Larynxödem)
    + Kreislaufzusammenbruch
    + Bewusstseinsverlust
+ Stadium 4:
    + Herz-Kreislauf-Stillstand

## Maßnahmen

+ Lagerung je nach Bedarf – flach bei Hypotonie, erhöht bei Atembeschwerden
+ O&#8322; Gabe
+ Monitoring
+ NA nachfordern wenn notwendig
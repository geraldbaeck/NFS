title:          Schock
date:           2019/01/06
version:        0.0.7
authors:        gerald@baeck.at
ankikat:        Notfälle
                Notfallmedizin
md5:            e50e53b28a6f707e39717e865829785e
date_modified:  2019/05/06

## Definition

+ = akute Unterversorgung lebenswichtiger Organe mit Blutsauerstoff
+ = absoluter oder relativer Volumenmangel
+ = ein Missverhältnis zwischen Blutvolumen und Gefäßkapazität

+ abhängig von:
    + zirkulierenden Blutmenge
    + Spannungszustand der Blutgefäße
    + Pumpfunktion des Herzens

## Arten

+ **hypovolämisch** = absoluter Blutvolumenmangel (zB Blutung, Verbrennung, Erbrechen, Durchfall, Schwitzen)
+ **kardiogen** = Versagen der Herzfunktion
+ **distributiv** = relativer Volumenmangel
    + ***anaphylaktisch*** = Maximalstadium einer Allergie, Histaminfreisetzung bei der Allergie bewirkt eine Vasodilatation
    + ***septisch*** = Toxine lösen eine Erschlaffung der Arterien aus
    + ***neurogen*** = Gestörter neuraler Kontrollmechanismus löst Erschlaffung der Arterien aus
+ **obstruktiv** = Blockade des Gefäßsystems va. der Hauptgefäße (zb Vena Cava Syndrom, Herzbeuteltamponade, Lungenembolie, Spannungspneumothorax)

## Verlauf

+ Minderdurchblutung löst **Zentralisierung des Kreislaufs** aus
+ Haut, Muskulatur, Darm und Niere werden von Durchblutung ausgeschlossen
+ dadurch entsteht ein Sauerstoffmangel in diesen Organen
+ führt zur Entgleisung des Stoffwechsels
  + => **Azidose**
  + => Zellschädigung
  + => (Multi)organversagen (Schocklunge, Schockniere)
  + => Tod

### Kompensierter Schock

= Körper nimmt den Kampf auf

+ Aktivierung des Sympathikus
    + Tachykardie
    + Schweiß
    + Verengung der peripheren Gefäße
+ Zentralisierung des Kreislaufs

### Dekompensierter Schock

Schließlich versagen alle Kompensationsmechanismen.

+ Weitstellung der Gefäße
+ Kapillaren werden undicht, es kommt zum Verlust von Flüssigkeit ins Gewebe
+ Organismus erhält zu wenig Sauerstoff (Hypoxie) und wird übersäuert (Azidose)
+ unkontrollierte Gerinnungsvorgänge => multiple Thrombosen
+ verbrauchte Gerinnungsfaktoren fehlen => Blutungen (intern)
+ Organe versagen (Schockniere, Schocklunge, ...)
+ MOV

## Schockindex

Schockindex = Puls / Blutdruck

## Schockzeichen

| Stadium | Haut | Puls | RR | Atmung | Pupillen | Bewusstsein |
| --- | --- | --- | --- | --- | --- | --- |
| `Anfangsstadium` | blass, kühl, Zittern | leicht erhöht (Ausnahme kardiogener Schock) | normal |  normal | normal | alert |
| `Bedrohliches Stadium` | kalter Schweiß | stark erhöht, schlecht tastbar | <90mmHg | flach und beschleunigt | normal | getrübt |
| `Fortg. Stadium` | grau (blau) | kaum tastbar | nicht messbar | evt Schnappatmung | weit, kaum Reaktion | bewusstlos |

## Maßnahmen

+ Schocklagerung (Ausnahme Bewusstlosigkeit)
+ Blutung stillen
+ O&#8322;
+ Wärmeerhalt
+ Psychische Betreuung
+ Monitoring
+ NEF
+ Venenzugang (großlumig, weiß aufwärts)
+ Volumen (Ausnahme: Kardiogener Schock)
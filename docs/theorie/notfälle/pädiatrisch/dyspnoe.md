title:          Dyspnoe beim Kleinkind
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle Kinder
                Notfallmedizin
md5:            5ee308bcf3830a20cbd369a5a8d4f520
date_modified:  2019/04/29
date:           2019/04/29

## Ursachen

+ Infektion der oberen Atemwege
+ Fremdkörperaspiration
+ Asthma bronchiale
+ Pneumonie
+ Keuchhusten beim Säugling

## Symptome

+ Einziehung
    + zwischen den Rippen
    + Drosselgrube
    + Rippenbogen
+ Nasenflügeln
+ Stridor
+ Exspiratorisches Giemen / Stöhnen
+ Tachypnoe

## Maßnahmen

+ 02-Gabe
+ Beruhigung der Eltern
+ Rascher Transport auf eine Kinderabteilung
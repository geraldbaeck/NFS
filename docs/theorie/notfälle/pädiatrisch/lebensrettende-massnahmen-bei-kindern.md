title:          Lebensrettende Maßnahmen bei Kindern
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Kinder
md5:            cc6d12060ed48d00f9cc30e2fd5878d2
date_modified:  2019/01/07

![Kinderreanimation](https://rawgit.com/geraldbaeck/RS_WRK/master/charts/Lebensrettende_Massnahmen_bei_Kindern.svg)Reanimationsalgorithmus bei Kindern[^1]
{: .imagewithcaption}

[^1]: [Gerald Bäck](mailto:gerald@baeck.at) with a [CC0-1.0](https://creativecommons.org/licenses/by-sa/2.0/at/deed.de) public domain dedication = no copyright
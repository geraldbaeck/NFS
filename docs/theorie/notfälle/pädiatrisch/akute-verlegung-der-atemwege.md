title:          Akute Verlegung der Atemwege
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Kinder
md5:            c187aff296a9e150a17370b6864a6e06
date_modified:  2019/01/07

![Akute Verlegung der Atemwege](https://rawgit.com/geraldbaeck/RS_WRK/master/charts/Kinder_VerlegungAtemwege.svg)
Algorithmus bei akuter Atemwegsverlegung[^1]
{: .imagewithcaption}

[^1]: Quelle: [Gerald Bäck](mailto:gerald@baeck.at) with a [CC0-1.0](https://creativecommons.org/licenses/by-sa/2.0/at/deed.de) public domain dedication = no copyright
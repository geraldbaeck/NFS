title:          Lebensrettende Maßnahmen beim Säugling
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Kinder
md5:            4eda1e22d1c6acd24c2ecd39db63390d
date_modified:  2019/01/07

![Säuglingsreanimation](https://rawgit.com/geraldbaeck/RS_WRK/master/charts/Lebensrettende_Massnahmen_beim_Saeugling.svg)Reanimationsalgorithmus bei Säuglingen[^1]
{: .imagewithcaption}

[^1]: [Gerald Bäck](mailto:gerald@baeck.at) with a [CC0-1.0](https://creativecommons.org/licenses/by-sa/2.0/at/deed.de) public domain dedication = no copyright
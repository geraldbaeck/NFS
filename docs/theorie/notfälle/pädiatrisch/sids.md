title:          SIDS
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle Kinder
                Notfallmedizin
md5:            3628a21f7eca9c6d852ee3815b69c03e
date_modified:  2019/04/28
date:           2019/04/28

## Definition

=Sudden Infant Death Syndrome (=plötzlicher Kindstod)

## Risikofaktoren

+ Geschwister die an SIDS verstorben sind 
+ Frühgeburt
+ Bauchlage im Schlaf
+ Rauchen; Alkoholkonsum der Eltern
+ Kein Stillen
+ Überwärmung (Hyperthermie)
+ Männliches Geschlecht

## Maßnahmen

+ CPR
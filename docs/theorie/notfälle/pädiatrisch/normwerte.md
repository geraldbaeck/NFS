title:          Normwerte
date:           2018/12/28
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle Kinder
                Notfallmedizin
md5:            a0e6c97c84b2ab22f23da0ec37cc439d
date_modified:  2019/05/27

| Alter | AF | AZV | HF | RR Systole |
| --- | --- | --- | --- | --- |
| `Neugeborenes`| 40-60 | 20-40 | 120 | - |
| `Säugling` | 20-30 | 50-100 | 120 | 60 |
| `Kind` | 15-20 | 100-200 | 100 | 90 |
| `Erwachsener` | 12-15 | 500 | 80 | 120 |
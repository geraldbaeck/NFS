title:          Traumatologische Kindernotfälle
date:           2019/01/06
version:        0.0.7
authors:        gerald@baeck.at
ankikat:        Notfälle Kinder
                Notfälle Trauma
                Notfallmedizin
                HeavyRotation
md5:            8a330f7b1d1f82633a83fe85966fa60d
date_modified:  2019/06/04

## Anatomische Besonderheiten

+ schlechtere Kraftableitung durch
    + weniger Körperfett
    + vermehrte Gewebeelastizität
    + weniger Raum für Organe
    + Knochen elastischer
    + => weniger Knochenbrüche, mehr Organverletzungen
+ schwieriger Atemweg
    + größerer Kopf u. Zunge => Lagerung schwierig
    + kürzere Trachea => Fehlintubation
    + häufiger Atmungsstörung als Kreislaufstörungen
+ Kreislauf
    + rasche Dekompensation, lange Kompensation
    + Hypoventilation und Hypoxie sind häufiger als Volumenmangel und Blutdruckabfall
+ indirekte Schockhinweise
    + ZNS: Verwirrtheit, Irritabilität, Bewusstseinsstörung
    + Haut: Rekapzeit verzögert, Hautfarbe, Temperatur

## Management nach PHTLS

+ SSS
    + andere "normale" Vitalparameter bei Kindern
+ GI
    + besonderes Augenmerk auf Kind 
    + => Pädiatrisches Dreieck (Atmung, Kreislauf, Erscheinungsbild)
    + vorsichtige Näherung
    + Entscheidung Kopf bis Fuß oder Fuß bis Kopf Untersuchung
+ A
    + schwieriger Atemweg beim Kind
    + Überstrecken ja nach Alter
    + Fremdkörperaspiration bedenken
+ B
    + Atemanstrengung beurteilen
    + Nasenflügeln?
+ C
    + rekap besondere Aussagekraft
    + Kinder kompensieren länger und besser
    + dekompensieren aber schnell
+ D
    + GCS nicht anwendbar, auf Altersstufe anpassen
+ E
    + Kinder kühlen schneller aus

+ SAMPLER
    + immer auch an Missbrauch denken
    + Lokalisierung von Beschwerden ist oft ungenau (Hüfte-Knie!!!)
    + Anamnese bei Kleinkindern schwierig

+ Transport
    + Kindertraumazentrum => AKH o. SMZ Ost
    + Kinderrückhaltevorrichtung verwenden

## Formeln

+ Gewicht: Alter*4 + 4
+ Tubus: Alter/4 + 4
+ Volumen: 20ml/kg KG
+ Blutmenge: 6-8% des KG
title:          Krupp
date:           2019/01/06
version:        0.0.5
authors:        gerald@baeck.at
ankikat:        Notfälle Kinder
                Notfallmedizin
md5:            36d0955edac1c4e3f224641c4942306b
date_modified:  2019/05/06

## Definition

= entzündliche Erkrankung des Kehlkopfes

Zwischen Epiglottitis (echter Krupp) und Pseudokrupp kann präklinisch schwer unterschieden werden.

| | Laryngitis | Epiglottitis |
| --- | --- | --- |
| Ursachen | Virusinfektion | bakteriell |
| Symptome | <ul><li>Heiserkeit bis zur Stimmlosigkeit</li><li>Schmerzen</li><li>Hustenreiz</li><li>typischer bellender Husten</li><li>meist in den Nachstunden u. sehr plötzlich</li></ul> | <ul><li>wirkt schwer Krank</li><li>hohes Fieber</li><li>Speichelfluss aus Mund<li></li>kann nicht richtig schlucken</li></ul> |
| Maßnahmen | <ul><li>Kalte-feuchte Luft</li><li>Beruhigen (Kind & Eltern)</li><li>Transport unter Beobachtung der Vitalwerte</li><li>evtl. 02-Gabe</li><li>NA (Kortison- Zäpfchen, Adrenalin ü. Verneblermaske)</li></ul> | <ul><li>keine Manipulation im Rachenraum</li><li>Beruhigen</li><li>Transport unter Beobachtung der Vitalwerte</li><li>02-Gabe</li><li>"Load and Go" in Reanimationsbereitschaft</li><li>bei Vitaler Bedrohung NA</li></ul> |

## Komplikationen

+ Bewusstlosigkeit
+ Ersticken
+ Atem-Kreislauf-Stillstand

[Krupp Hörbeispiel](https://www.youtube.com/embed/Qbn1Zw5CTbA?list=PLhC2h1cbsdE1TUa2uDimZ--EWI9g-eeOm){: .youtube}
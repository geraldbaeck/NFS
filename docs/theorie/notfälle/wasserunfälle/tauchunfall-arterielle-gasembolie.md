title:          Tauchunfall - Arterielle Gasembolie
des:            Verstauchung
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Wasserunfälle
md5:            39c0cc94d06e013fe23183962b43aec4
date_modified:  2019/01/07

## Definition
= Gasbläschen im Gefäßsystem sorgen für embolisches Geschehen

## Ursachen
+ [Dekompressionskrankheit](tauchunfall-dekompressionskrankheit.md) 
+ über die Lungenvenen ins Gefäßsystem ([Barotrauma](tauchunfall-barotrauma.md))

## Komplikationen
+ Bereich des Gerhirns => Schlaganfallsymptome
+ Bereich der Herzkrankgefäße => Herzinfarktsymptome
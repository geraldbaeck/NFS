title:          TauchTauchunfall - Rettung
des:            Verstauchung
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Wasserunfälle
md5:            bc56a6444899efe9982b25bb5be082ca
date_modified:  2019/01/07

## Maßnahmen
+ Bleigurt entfernen
+ Tarierweste entfernen
+ Tauchcomputer sicheren
+ Flache Lagerung auf weichen Untergund, Druckstellen vermeiden
+ Maximale Sauerstoffgabe
+ Anleitung zur Atmung
+ Wärmeerhalt
+ Flüssigkeit zum Trinken anbieten
+ evt Deko Kammer rechtzeitig avisieren

## Anamnese
+ Tauchcomputer
+ Alle in den letzten 24h durchgeführten Tauchgänge ermitteln (Nullzeitüberschreitungen, Dekostopps, Tauchtiefen, Tauchdauer)
title:          Extremitätentrauma
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Trauma
md5:            cd216e68d01b36bc13ff0a70b4d044dc
date_modified:  2019/01/07

## Arten  
+ Quetschungen, Prellungen
+ Meniskusverletzungen
+ Muskelzerrungen, Muskelrisse
+ Sehnenzerrungen, Sehnenrisse
+ Bänderzerrungen, Bänderrisse
+ Gelenksverletzungen:
    + Verstauchung (Distorsion)
    + Verrenkung (Kontusion)
    + Bandverletzungen
    + Kapselverletzungen
+ Knochenbrüche
+ Gefäßrupturen


## Algorithmus  
+ Vorher Lebensfunktionen sicherstellen
+ Blutungen stillen
+ betroffenen Extremität soll möglichst ohne Unterbrechung unter Zug gehalten werden
    + Zug und Gegenzug am Arm
    + Stiefelgriff am Bein
+ immer Schmuck entfernen
+ Beinbruch: Schuhe unter Zug ausziehen
+ Kleidung entfernen oder aufschneiden
+ anschließened MDS Kontrolle
+ Offene Brüche keimfrei verbinden, Fremdkörper fixieren
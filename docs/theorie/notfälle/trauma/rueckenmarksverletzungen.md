title:          Rückenmarksverletzungen
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Trauma
                Notfallmedizin
md5:            097ab8e7a52bb310a7efca38995df949
date_modified:  2019/05/03
date:           2019/04/22

## Unfallmechanismen

+ haupts. stumpfes Trauma
+ Verkehrsunfälle
+ Motorradunfall
+ Sturz aus großer Höhe
+ Kopfsprung in seichtes Wasser
+ allgemein große Gewalteinwirkung auf Kopf, Hals, Thorax und Becken
+ aus dem Fahrzeug geschleudert
+ nach Auffahrunfällen
+ Kletterunfall

## Indizien

+ Bruch der Fersenbeine => dann wahrscheinlich Sturz von größerer Höhe
+ Bruch des Brustbeines => wenn die vordere Säule gebrochen ist,......
+ Gedeckte Aortenruptur => weist auf massive Entschleunigung hin
+ Tod eines anderen Fahrzeuginsassen
+ Schäden am Sturzhelm
+ Verletzung an der Stirn oder der vorderen Schädeldecke => Dens (2. HW) od HWS Verletzung

## Symptome

+ Schlaffer Muskeltonus
+ Fehlende Abwehrreaktion auf Schmerzreize
+ Auffällige Bewegungs-/Reflexdifferenz OE+UE
+ Reine Bauchatmung
+ Priapismus

## Ablenkungen

+ Alkohol oder andere Drogen
+ Stark blutende Verletzungen
+ Frakturen großer Röhrenknochen
+ Verbrennungen
+ Vermutete innere Verletzung
+ Kommunikationsprobleme

## Pathophysiologie

+ primäre Verletzung durch direkte Gewalteinwirkung
    + Commotio spinalis (Erschütterung)
        + wird wieder gut
        + vorübergehende Störung, Lähmung bis 48h
    + Contusio spinalis (Prellung)
        + wird vielleicht wieder
        + neurologische Ausfälle, manchmal verzögert
    + Compressio spinalis (Quetschung)
        + wird nicht mehr
        + schwerste Verletzung
        + oft bei Wirbelfrakturen
+ sekundäre Verletzung durch Ischämie, Schwellung, Bewegung von Knochenfragmenten
    + hoher Querschnitt (über C4)
        + auch Beeinflussung von Atmung,
        + Kreislauf bis Schock => Spinaler Schock
    + tiefer Querschnitt
        + motorische Ausfälle mit Sensibilitätsstörung (Arme, Beine)

## PHTLS Management

+ SSS
    + Eigenschutz
    + Rettungsmittel
+ GI
    + Unfallmechanismus
    + Crash Rettung?
    + Sauerstoff
+ ABC
+ D
    + neurologische Ausfälle nicht zwingend nötig
    + Querschnittshöhe?
+ E
    + Wärmeerhalt
    + ablenkende Verletzungen?
    + Ganzkörperuntersuchung

+ Immobilisation
    + stabile Seitenlage ist kontraindiziert
    + ggf. Helmabnahme
    + Kopf- Fixierung in neutraler Position (MILS)
    + Ganzkörperimmobilisation, (Spineboard, Vakuum)

+ Transport
    + regelm. Evaluierung Kreislauf u. Neurologie 
    + Schmerztherapie
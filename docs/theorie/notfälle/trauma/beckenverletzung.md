title:          Beckenverletzung
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Trauma
md5:            889842d3c9ec6c49253f7727830d296a
date_modified:  2019/01/07

## Definition  
Durch Verkehrsunfälle, Sturz aus großer Höhe. Knöcherne Strukturen, große Gefäße, Nerven und im Becken liegende Organe können verletzt werden. Beckenverletzungen können präklinisch nur auf Grund er Unfallmechanik und der Schmerzsymptomatik vermutet werden.

## Symptome  
+ Schmerzen
+ Bewegungseinschränkung

## Komplikationen  
+ starke (innere) Blutung

## Maßnahmen  
+ (HWS-Schiene), Schaufeltrage, Vakuummatratze
+ allg. Maßnahmen
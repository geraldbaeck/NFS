title:          Blutverlust bei Frakturen
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Notfälle Trauma
                Notfallmedizin
md5:            572cea624568575b66f87b979fe2802b
date_modified:  2019/04/22
date:           2019/04/22

| Knochen | Menge |
| --- | --- |
| Oberarm (Humerus) | 100-800 ml |
| Unterarm | 50-400 ml |
| Becken | 500-5000 ml |
| Oberschenkel (Femur) | 300-2000 ml |
| Unterschenkel | 100-1000 ml |
title:          Polytrauma
des:            Quetschung, Prellung
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle Trauma
md5:            2e88dc794c13e2ecf8d734580771f496
date_modified:  2019/01/07

## Definition  
Gleichzeitige Verletzung mehrerer Körperregionen oder Organe, wobei midestens eine oder die Kombination lebensbedrohlich ist.

## Komplikationen  
+ Atem-Kreislaufstillstand

Insbesondere verdienen die "5 tödlichen Hypotheken" nach Dinkel eine besondere Beachtung  

+ Hypoxie (Sauerstoffmangel)
+ Hypovolämie (Volumenmangel)
+ Hypoperfusion (Minderdurchblutung)
+ Hypothermie (Wärmemangel)
+ Hypotherapie (stellt die Gefahr durch einen zu zaghaften oder zu zögerlichen Notarzt dar. Kaum eine notärztliche Maßnahme richtet - korrekt ausgeführt - wesentlichen Schaden an, auch wenn sie sich im nachhinein als „überflüssig“ herausstellen sollte.)

## Maßnahmen  
+ Kontrolle der Lebensfunktionen => ABC Permanent
+ lebensrettende Sofortmaßnahmen
+ Blutstillung
+ SPO&#8322;-Gabe 10-15l
+ Absaugbereitschaft
+ Zeit im Auge behalten
+ Schneller Transport in Klinik mit Schockraum

## Anamnese  
Wichtig ist, dass die Diagnose Polytrauma möglichst schnell erkannt und genannt wird. Die Arbeitsdiagnose Polytrauma wird gestellt sobald die  

+ Einsatzmeldung und/oder die
+ Unfallmechanik und/oder die
+ Vitalparameter

darauf schließen lassen.
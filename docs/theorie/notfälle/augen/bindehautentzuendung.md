title:          Bindehautentzündung
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Augen
                Notfallmedizin
md5:            64691cc71a5fc1dd5684528124ff915d
date_modified:  2019/05/08
date:           2019/05/06

## Arten und Ursachen

+ mechanisch
    + Rauch, Staub, trockene Luft (Conjunctivitis simplex acuta)
    + Trockene Bindehautentzündung (Keratoconjunctivitis sicca)
    + Gigantopapilläre Konjunktivitis (durch Kontaktlinsen)
+ bakteriell
    + unspezifische Kokken
    + Gonokokken
    + Diphtherie
+ viral
    + Keratoconjunctivitis epidemica (hochansteckend)
    + Akute hämorrhagische Bindehautentzündung
+ durch Pilze (Mykosen)
+ allergisch
+ durch Parasiten

+ Akut: Infektion der Bindehaut durch Bakterien, Viren (Keratokonjunktivitis epidemica sehr ansteckend!)
+ Chronisch: Reize wie Staub, Gase, Allergien

## Symptome

+ Rötung der Bindehaut (Hyperämie)
+ Bindehautschwellung (Chemosis)
+ Verengung der Lidspalte
+ vermehrter Tränenfluss (Epiphora)
+ Juckreiz
+ Fremdkörpergefühl
+ Brennen und teilweise starke Schmerzen

## Maßnahmen

+ Keine Notfall- Maßnahmen
+ Überweisung an Fachabteilung
+ Auf Hygiene achten!
+ Nach Kontakt unbedingt Hände waschen
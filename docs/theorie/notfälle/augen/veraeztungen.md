title:          Verätzungen des Auges
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Augen
                Notfallmedizin
md5:            6ed0b0301047abd370571f70fedcc611
date_modified:  2019/05/03
date:           2019/05/03

## Definition

=Schädigung der Horn- oder Bindehaut durch

+ Säuren
+ Laugen
+ feste Stoffe
    + z. B. Kalk

## Symptome

+ Bindehaut: Rötung bis Nekrose
+ Hornhaut: Epitheldefekte, Trübungen
+ "gekochtes Fischauge"

## Komplikationen

+ Erblindung infolge ausgedehnter Narben der Bindehaut und Hornhaut
+ Sekundärglaukom

## Maßnahmen

+ sofortige, ausgiebige Spülung mit reichlich Flüssigkeit (Physiologische Kochsalzlösung, Ringerlösung, Leitungswasser) für mindestens 10 Minuten
+ Entfernung der Ätzsubstanz am Unfallort

Sind unbedingte Maßnahmen, auch wenn das Ausmaß der Verätzung zunächst nicht bedrohlich erscheint.

+ das gespülte Auge sollte tiefer als das nicht- gespülte liegen und sollte in alle Richtungen schauen.
+ danach Verband auf beide Augen und
+ Transport auf eine Augenabteilung
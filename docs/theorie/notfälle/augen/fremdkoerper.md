title:          Fremdkörper im Auge
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle Augen
                Notfallmedizin
md5:            4d3bde11199876b7d94743adb3a3ec45
date_modified:  2019/05/03
date:           2019/05/03

## Symptome

+ Fremdkörpergefühl
+ Zwinkerzwang
+ Tränenfluss

## Maßnahmen

+ Fremdkörper mit freiem Auge sichtbar locker im Bindehautsack
    + mit einem Wattestäbchen in Richtung inneren Augenwinkel (= Richtung Nase) herauswischen
    + oder spülen
+ aber im Zweifelsfall
    + steriler, trockener Verband über beide Augen
    + Transport an eine Augenabteilung!!!!
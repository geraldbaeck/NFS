title:          Augenperforation
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle Augen
                Notfallmedizin
md5:            3cd2fc01aa94be0ce4e6e9ad10b98baa
date_modified:  2019/05/06
date:           2019/05/06

## Definition

+ = Verletzung mit Eröffnung des Augapfels

## Ursachen

+ spitze bzw. scharfe Gegenstände wie Messer oder Glasscherben
+ Arbeiten mit Hammer und Meißel ohne Schutzbrille

## Symptome

+ Schmerzen
+ Eingeschränktes Sehvermögen

## Komplikationen

+ Endophthalmitis durch Fremdkörper (=akute bakterielle intraokulare Infektion: Glaskörperabszess)

## Maßnahmen

+ Allfällige Fremdkörper belassen
+ abpolstern
+ beidseitiger Verband
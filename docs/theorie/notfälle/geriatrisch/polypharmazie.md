title:          Polypharmazie
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Geriatrisch
md5:            02c2ef3c4275c04cd14b1149353535fe
date_modified:  2019/01/07

## Definition
= die gleichzeitige Einnahme von mindestens 5 Medikamenten (Quelle: WHO)

## Ursachen
+ Multimorbidität = gleichzeitig mehrere behandlungsbedürftige Erkrankungen
+ unzureichende Wirkung eines Medikaments => Kombination mehrer Medikamente
+ Medikament ist nötig, um Nebenwirkungen eines anderen abzuschwächen

## Komplikationen
+ potenzierte Wechselwirkungen
+ Delir
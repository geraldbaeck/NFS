title:          Traumatologische Notfälle beim geriatrischen Patienten
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle Geriatrisch
                Notfallmedizin
md5:            7d437556f5b48a77eefc365d4ba3b396
date_modified:  2019/04/22
date:           2019/04/22

## Auswirkungen des Altern

+ Anatomie
    + Muskuloskelettales System: Dehydration, Knochen brüchiger
    + verminderte Muskelkraft (Sarkophenie)
    + WS-Veränderung, Rückenmark weniger geschützt
+ Atmung
    + Ventilation wird schlechter
        + Brustwand wird steifer
        + verminderte Elastizität der Alveolen => weniger Gasaustauschfläche
        + Fähigkeit zur Sauerstoffsättigung des Hämoglobins sinkt
    + Zahnlosigkeit erschwert die Beatmung
    + Erhöhtes Aspirationsrisiko (schlechtere Abwehrreflexe)
+ Kreislauf
    + Schlechtere Kreislaufsituation begünstigt Hypoxie
        + Vorbestehende kardiovaskuläre Erkrankung
        + Herzinsuffizienz
    + Herzfrequenz und RR nicht so aussagekräftig
    + Schockzeichen werden oft spät erkannt
+ ZNS
    + Atrophie des Gehirns
    + Verminderte Sinneswahrnehmung
    + Mentale und psychomotorische Leistung sinkt
    + Schlechte Temperaturregulation, schnelle Auskühlung
    + Wahrnehmung vermindert
        + 28% haben Hörstörungen
        + 13% haben Sehstörungen
        + Schmerzwahrnehmung ist verändert

+ Lagerung
    + Anpassen der Lagerung, da die anatomische Körperhaltung nicht mehr ganz „normal“ sein kann.

## Management nach PHTLS

+ SSS
    + immer auch an versteckte Ursachen bzw. Komorbiditäten denken => interne Auslöser?
+ A
    + Zahnersatz beachten
+ B
    + Brustkorb steifer
    + COPD?
    + Pneumonierisiko höher
+ C
    + Herzfrequenz und RR nicht so aussagekräftig
    + Schockzeichen werden oft spät erkannt
    + Blutverdünner?
+ D
    + neurologische Situation vor dem Unfall berücksichtigen
+ E
    Schnellere Auskühlung

+ S
    + Leitsymptom nicht immer eindeutig
+ A
+ M
    + Ganze Liste erheben/mitnehmen
    + Speziell auf Blutverdünner achten
+ P
    + Vorbefunde
+ L
    + Wichtig letzter Spitalsaufenthalt
+ E
+ R
title:          Der Geriatrische Notfall
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Geriatrisch
md5:            b895ec34be49e3cf2427c864c93f027b
date_modified:  2019/01/07

## Symptome
+ Symptome des akuten Notfalls
+ Symptome der chronischen Erkrankung
+ CAVE: Vermischung möglich

## Maßnahmen
+ Sanitätshilfliche Maßnahmen
+ Beurteilung der Akutsymptome und deren möglicher Zusammenhang mit chronischen Erkrankungen
+ SAMPLE

## Komplikationen
+ chronische Symptome könnne vom Akutereignis ablenken
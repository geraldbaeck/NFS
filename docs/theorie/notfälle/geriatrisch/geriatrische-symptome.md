title:          Geriatrische Symptome
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Geriatrisch
md5:            4a514643839ecbe30ffa15a51acc5d62
date_modified:  2019/01/07

## Symptome
+ kardiale Instabilität
+ eingeschränkte Atemfunktion (zb COPD)
+ Inkontinenz
+ kognitive Defizite bis zur Demenz
+ Delir
+ Mangelernährung
+ Verdauungsstörung, Obstipation (Verstopfung)
+ Sarkopenie (Verlust an Muskelmasse), Mobilitätseinschränkungen, Sturzneigung
+ Frailty (multifaktorielles geriatrisches Syndrom: Schwäche, Gewichtsabnahme, Gangunsicherheit...)
+ Soziale Isolation, Depression
+ Chronischer Schmerz

## Erkrankungen
+ Hypertonie
+ Diabetes mellitus
+ COPD
+ Krebs
+ Durchblutungsstörungen
+ Herzrhythmusstörungen
+ Neurodegenerative Erkrankungen (Parkinson)
+ Engpasssyndrome
+ Arthrosen
+ Schilddrüsenerkrankungen
+ Schwerhörigkeit
+ Sehschwäche
title:          Atmungsorgane
date:           2019/01/06
version:        0.0.6
authors:        gerald@baeck.at
ankikat:        Anatomie
                Notfallmedizin
md5:            fb598b4259956b0362b4465d944bf026
date_modified:  2019/04/20

+ Nase
    + Riechen
    + Anfeuchten
    + Erwärmen
    + Filtern
+ Nebenhöhlen
    + Stirnbeinhöhlen
    + Keilbeinhöhlen
    + Siebbeinhöhlen
    + Kieferhöhlen
    + Aufgabe:  
        + Gewichtsreduzierung
        + Oberflächengewinn
+ Pharynx (Rachen)
    + Weiche zw Luft- und Speiseröhre
    + Mandeln für Immunabwehr
+ Larynx (Kehlkopf)
    + verschließt Luftröhre beim Schlucken
    + Stimmbildung
+ Trachea (Luftröhre)
    + Knorpelspangen => Stabilität
    + Muskelschicht => Flexibilität
+ Lunge
    + rechts drei Lappen
    + links zwei Lappen
    + wird von den Rippen geschützt
    + Rippenfell < Pleuraspalt > Lungenfell
    + Bronchien
        + links 2 Hauptbronchien, rechts 3
        + verteilt die Luft in der Lunge, verästelt sich mikroskopisch
        + Festkörper landen meistens rechts
    + Alveolen (Lungenbläschen)
        + die "Sackgasse" der Lunge
        + Gasaustausch = Abgabe von Sauerstoff ins Blut und CO&#8322; aus dem Blut

![Atemsystem erklärt 1](https://www.youtube.com/watch?v=lqxkNLZeQv4)
title:          Ohr
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Anatomie
                Notfallmedizin
md5:            28a031bc9a60152920bcc43b231c4081
date_modified:  2019/05/06
date:           2019/03/15

## Aufbau

+ äußeres Ohr
    + Ohrmuschel
    + äußerer Gehörgang
+ Trommelfell
+ Mittelohr (Paukenhöhle)
    + Knöchelchen
        + Hammer
        + Amboss
        + Steigbügel
        + geben den Schalldruck ans Innenohr weiter
    + [rundes](https://de.wikipedia.org/wiki/Fenestra_cochleae), ovales Fenster
    + Ohrtrompete, Eustachische Röhre (Verbindung zum Rachen) => Druckausgleich
+ Innenohr
    + Gehörschnecke => Umwandlung Schall in Nervenimpulse
    + Bogengänge => VIII. Hirnnerv (Nervus vestibulocochlearis) Nerv des Vestibularorgans => Gleichgewicht
    + Hörnerv

![Das menschliche Ohr](https://upload.wikimedia.org/wikipedia/commons/9/9a/%C3%84u%C3%9Feres_Ohr_-_Mittelohr_-_Innenohr.jpg)
Das menschliche Ohr[^1]
{: .imagewithcaption}

[^1]: [Geo-Science-International / Wikimedia Commons](https://commons.wikimedia.org/wiki/File:%C3%84u%C3%9Feres_Ohr_-_Mittelohr_-_Innenohr.jpg), [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en)
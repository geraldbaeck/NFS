title:          Blut
date:           2019/01/06
version:        0.0.7
authors:        gerald@baeck.at
ankikat:        Anatomie
                Notfallmedizin
md5:            b2461915477ec61a352d9f021c3155f1
date_modified:  2019/06/13

## Bestandteile

+ 55-60% Flüssigkeit (Blutplasma)
    + Wasser
    + Fibrinogen
    + Eiweiße
    + Elektrolyte
    + Kohlenhydrate
    + Fette
    + Hormone
    + Vitamine
    + Stoffwechselprodukte
+ 40-45% feste Bestandteile (Hämatokrit)
    + Erythrozyten (rot)
    + Leukozyten (weiß) 1%
    + Thrombozyten (Blutplättchen)

## Aufgaben

+ **Transport** von Sauerstoff, Wasser Hormonen usw. zu den Zellen
+ **Abtransport** von Kohlendioxid, Abfallprodukten zu Ausscheidungsorganen (Lunge, Niere)
+ **Gerinnungsfunktion** bei Verletzung
+ **Versorgung mit Abwehrstoffen** und Schutz vor Infektionen
+ **Pufferfunktion** bei Schwankungen des pH-Wertes
+ Träger der Blutgruppeneigenschaften
+ ca. 5 Liter beim Erwachsenen
+ pH Wert 7,4 (<7,35 Azidose/Übersäuerung, >7,45 Alkalose/Säuredefizit)
title:          Stoffwechsel
date:           2018/12/22
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            c9f94e0c85305f311d0f197832063526
date_modified:  2019/03/15

## Definition

Dient der Verwertung von Nahrungsbausteinen (Eiweiße, Fette, Kohlehydrate) die durch Enzyme ab- und umgebaut werden, um Körperzellen notwendige Energie zur Verfügung zu stellen. Energie wird freigesetzt = Verbrennung

+ Zerlegung der Nahrung
+ Bereitstellung von Brenn- und Baustoffen
+ Regulation der Konzentration im Blut

## Bestandteile

+ Kohlehydrate
    + Zucker (Glucose)
    + Stärke (Mehrfach-Zucker)
    + => Energie
+ Lipide (Fette)
    + Triglyceride, Cholesterin
    + Energie, Baustoff für Membranen
+ Proteine (Eiweiß)
    + Baustoff

+ Häufigste Stoffwechselerkrankung = Diabetes mellitus
+ Gicht
+ Hashimoto
title:          Osmose
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Anatomie
                Notfallmedizin
md5:            0eb88974e22fd7524880bc7c00d065b6
date_modified:  2019/05/18
date:           2019/04/20

## Definition

= Diffusion einer Flüssigkeit durch eine selektiv-permeable Membran entlang eines Konzentrationsgefälles der Lösungsmittel im Bezug auf die gelösten Teilchen. Die Diffusion erfolgt in Richtung der höheren Konzentration.

Durch die Diffusion in die Richtung der höheren Konzentration steigt die Flüssigkeitssäule auf dieser Seite. Die Flüssigkeitssäule erzeugt einen hydrostatischen Druck, der auf die Membran wirkt. Dieser Druck wirkt dem Verdünnungsbestreben (=**osmotischer Druck**) entgegen.

## Osmotischer Druck

![Osmotischer Druck in einem U-Rohr](https://upload.wikimedia.org/wikipedia/commons/8/81/Osmotischer_Druck_in_einem_U_Rohr.svg)
Links ein U-Rohr mit verschieden konzentrierten Lösungen unmittelbar nach dem Einfüllen, rechts dasselbe U-Rohr zu einem späteren Zeitpunkt. Das Gleichgewicht ist erreicht, wenn der hydrostatische Druck der rechten Flüssigkeitssäule dem osmotischen Druck der Lösung entspricht.[^1]
{: .imagewithcaption}

[^1]: [Johannes Schneider / Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Osmotischer_Druck_in_einem_U_Rohr.svg), [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en)

Die Maßeinheit für den Osmotischen Druck ist Osmol pro Liter (osm/l) = Menge der gelösten Teilchen pro Liter Lösung in Mol. Er ist gleich dem Druck, den der gelöste Stoff ausüben würde, wenn seine Moleküle als Gas in dem von der Lösung eingenommenen Raum vorhanden wären. 

Der Osmotische Druck im Blutplasma liegt bei 300 mosm/l (milliosmol/Liter).

## Beispiele

### Durstgefühl

Der Körper reguliert seinen Flüssigkeitshaushalt über die Menge der Urinausscheidung und über den Durst. Starker Durst zeigt an, dass der Körper mehr Flüssigkeit benötigt, weil die Salzkonzentration im Körper zugenommen oder das Flüssigkeitsvolumen abgenommen hat.

### Diabetes

+ Teilchenzahl im Plasma durch den Zucker stark erhöht
+ Durstgefühl und Trinken großer Flüssigkeitsmengen
+ Hyperosmolares diabetisches Koma

### Laxantien

Osmotische Laxantien (Bittersalz, Glaubersalz, Laktulose) halten Wasser im Darm zurück, der Darminhalt bleibt weicher und wird leichter
ausgeschieden.

### Infusionen

+ Hypotone Lösung
    + "z.B. Ringerlactat ca. 278mosm/l"
    + Weniger gelöste Teilchen wie im Blutplasma
    + Flüssigkeit wird von Gefäßen ins Gewebe gezogen
    + Nicht bei SHT verwenden (Ödembildung)
+ Isotone Lösung
    + "z.B. 0,9% NaCI 308mosm/L oder Elo-Mel 302mosm/L"
    + Gleicher osmotischer Druck zw. zwei Flüssigkeiten
+ Hypertone Lösung
    + "z.B. Hyperhaes 2566mosm/L"
    + Glucose 10% 555 mosm/l
    + Höherer osmotischer Druck wie im Blutplasma
    + Zieht Flüssigkeit in die Gefäße
    + Vermeidet Ödembildung
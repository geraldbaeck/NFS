title:          Entzündung
authors:        gerald@baeck.at
ankikat:        Anatomie
version:        0.0.1
md5:            1cd5bdae0c0f120a99d62b60f93ca658
date_modified:  2019/03/15
date:           2019/03/15

## Definition

= Abwehrmechanismus des Körpers

## 5 Kardinalzeichen der Entzündung (nach Galen)

+ **Dolor**: Schmerz
+ **Tumor**: Schwellung, Ödembildung (=Gefäße werden undicht)
+ **Rubor**: Röte
+ **Calor**: Wärme
+ **Functio laesa**: eingeschränkte Funktion
title:          Milz
date:           2018/12/22
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            58597bced00749a73ba6472d278b26a6
date_modified:  2019/03/15

+ Liegt im linken Oberbauch, geschützt unter dem Rippenbogen
+ Ein Teil der weißen Blutkörperchen wird hier gebildet (bei Embryos und Kleinkindern) und gespeichert
+ Rote Blutkörperchen werden abgebaut und ins Blut eingedrungene Krankheitserreger abgewehrt = Friedhof der Erethrozyten
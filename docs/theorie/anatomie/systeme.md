title:          System
date:           2019/03/07
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            62bd5b36afd14854619886efe707c682
date_modified:  2019/03/15

## Definition

System erstrecken sich über den ganzen Körper oder zumindest Teile davon

+ Skelett
+ Muskeln
+ Nervensystem
+ Verdauungssystem
+ Kreislauf
    + Herz
    + Gefäße
    + Blut
+ Drüsen
+ Harnsystem
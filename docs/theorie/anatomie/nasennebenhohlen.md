title:          Nasennebenhöhlen
date:           2018/12/22
version:        0.0.2
authors:        gerald@baeck.at
md5:            9abb2c0df55ad4bfd053ecb5c994166f
ankikat:        Anatomie
date_modified:  2019/01/07

+ Stirnbeinhöhlen
+ Keilbeinhöhlen
+ Siebbeinhöhlen
+ Kieferhöhlen
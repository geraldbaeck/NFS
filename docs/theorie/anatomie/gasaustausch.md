title:          Gasaustausch
desc:           Innere und Äußere Atmung
date:           2019/01/06
version:        0.0.7
authors:        gerald@baeck.at
ankikat:        Anatomie
                Notfallmedizin
md5:            cec85c23cba1b844d2435bfab3f89e2b
date_modified:  2019/06/06

## Diffusion

+ physikalisches Prinzip: Partialdruckdifferenz (O&#8322; und CO&#8322;) = Konzentrationsgefälle
+ Partialdruck = Teildruck in einem Gasgemisch (=Konzentration)
+ Moleküle wandern immer vom höherem zu niedrigerem Partialdruck

Faktoren:

+ große Austauschfläche
+ kurzer Diffusionsweg
+ lange Kontaktzeit
+ großer Konzentrationsunterschied

Partialdruck:

+ Alveolen (O&#8322; 100mmHg, CO&#8322; 40mmHg)
+ Kapillaren (O&#8322; 40mmHg, CO&#8322; 46mmHg)

## Äußere Atmung (Lungenatmung)

+ mechanische Atmung
+ O&#8322; gelangt in die Lungenbläschen
+ O&#8322; wird ins Blut abgeben und an die roten Blutkörperchen (Erethrozyten) abgegeben und im Hämoglobin gebunden
+ CO&#8322; wird in die Alveolen abgegeben und abgeatmet
+ Blut-Luft-Schranke
    + Alveolarepithel
    + Basalmembran
    + Kapillarendothel

## Innere Atmung (Zellatmung)

+ =biochemische Energieumwandlung (Oxidation) des Sauerstoffs in den Zellen
+ Gasaustausch im Gewebe in den Kapillargefäßen
+ Nährstoffmoleküle in den Zellen reagieren mit O&#8322; und setzen Energie frei
+ Abbauprodukt CO&#8322; geht ins Blut (gelöst oder Bikarbonat)

[Gasaustausch](https://www.youtube.com/watch?v=jnuxgbACsCo){: .youtube}
[Blutgasanalyse: Partialdrücke leicht erklärt](https://www.youtube.com/watch?v=UG0G1Y_xR4o){: .youtube}
title:          Sinnesorgane
date:           2018/12/22
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            34807665ebbf6c2083c04a76ae38ead2
date_modified:  2019/01/07

+ Über **Haut wird Temperatur und Schmerz** wahrgenommen.
+ **Sehen**: Großteil der Wahrnehmung wird vom Sehsinn beeinflusst. Über Auge werden Informationen aufgenommen und über Sehnerv an Gehirn weitergeleitet. Augapfel ist in vorderen Teil der Augenhöhle eingebettet und wird vom Augenlid geschützt.
+ **Hören**: Neben Gehörorgan befindet sich auch Gleichgewichtsorgan im Ohr.
+ **Riechen und Schmecken**: Durch Nase wird nicht nur Atemluft aufgenommen, sondern auch Gerüche. Geschmacksrezeptoren sitzen auf Zunge. Erst in Verbindung mit Riechrezeptoren können über salzig, sauer, bitter und süß hinausgehende Eindrücke verarbeitet werden.
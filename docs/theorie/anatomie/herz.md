title:          Herz
date:           2019/01/06
version:        0.0.7
authors:        gerald@baeck.at
ankikat:        Anatomie
                Notfallmedizin
md5:            848859abb10efa76fbf97d609b91eb57
date_modified:  2019/04/20

## Definition

+ Zentralorgan des Kreislaufsystems (faustgroßer Hohlmuskel)
+ wirkt wie Druck-Saug-Pumpe, die Blut durch Körper befördert

## Aufbau

+ Liegt in der Mitte des Brustkorbs, hinter dem Brustbein.
+ Zwei Drittel etwas nach links verschoben.
+ Schichten (innen nach außen)
  + Endokard
  + Myokard (Muskel)
  + Epikard
  + seröser Spalt / Perikardhöhle (mit Flüssigkeit gefüllt)
  + Perikard (Herzbeutel)
+ Durch Klappen wird Fließrichtung bestimmt.
+ Durch längsgestellte Scheidewand (Septum) wird Herz in zwei Hälften geteilt.
+ Weitere Scheidewände trennen Herz in Vorhöfe und Kammern.
+ Vorhofmuskulatur viel geringer als Kammermuskulatur entwickelt.
+ Linke Herzkammer stärker als rechte (Lungenkreislauf).
+ Zwischen Vorhöfen und Kammern befinden sich Klappen.

## Klappen

+ gewährleisten, dass das Blut nur in eine Richtung fließen kann
+ bestehen aus Bindegewebe

+ Segelklappen (Atrio-Ventrikulär-Klappen)
  + rechts RVH/RK Trikuspidalklappe (3 zipfelig)
  + links LVH/LK Mitralklappe (2 zipfelig)
+ Taschenklappen
  + Pulmonalklappe RK/Lunge
  + Aortenklappe LK/Aorta

## Gefäße des Herzens

Herzmuskel wird durch Herzkranzgefäße aus großer Körperschlagader (Aorta) ernährt. Schlechte Durchblutung eines Herzkranzgefäßes führt zu Schmerzen (Angina Pectoris), Verschluss eines Herzkranzgefäßes führt zum Absterben von Herzmuskelgewebe (Herzinfarkt).

## Reizbildung und Reizleitung

Herzmuskel (Myokard) benötigt elektrischen Impuls, um sich zu kontrahieren. Herz besteht aus speziellem Muskelgewebe (Schrittmacherzellen), das in der Lage ist, eigenständig Reize zu bilden und weiterzuleiten. Anzahl der Herzschläge wird von vegetativen Nervensystem beeinflusst.

+ **Sinusknoten**
  + Frequenz von 60-80 / min
  + Erregung während der späten Füllungsphase über rechten Vorhof in Richtung Herzspitze
  + Signale bewirken Füllung der beiden Vorhöfe und der Kammern
+ **Internodalbahnen (Vorhofbahnen)**
+ **AV-Knoten (Atrioventrikular)**
  + Fällt Sinusknoten aus, übernimmt mit Frequenz von 40-60 / min
  + Die einzige elektrische Verbindung zwischen Vorhöfen und Kammern
  + besitzt mit 0,04–0,1 m/s die mit Abstand geringste Leitungsgeschwindigkeit des Herzens. So wird die Erregung mit starker Verzögerung auf die Kammern übertragen
+ **His-Bündel**
  + Ausfall des Sinus- und der AV-knoten => eigener Rhythmus von 20–30 / min (Kammerersatzrhythmus)
+ **Tawara-Schenkel**
+ **Purkinje-Fasern**

## Herzfrequenz

Gibt die Schläge pro Minute an. Normaler Puls bei Erw: 60-100/min.

**Tachykardie**: über 100/min, **Bradykardie**: unter 60/min
{: .info}

## Blutdruck

Wird durch die Tätigkeit des Herzens erzeugt, abhängig von:

+ Schlagkraft des Herzens
+ Blutmenge
+ Elastizität bzw Widerstand der Arterien

**Systole**: Druck beim Zusammenziehen des Herzens

**Diastole**: Druck beim Erschlaffen des Herzmuskels

## Werte

| &nbsp; | systolisch (mmHg) | &nbsp; | diastolisch (mmHg) |
| --- | :---: | :----: | :---: |
| Hypotonie | < 100 | oder | < 60 |
| Normotonie | 100-140 | und | 60-90 |
| Hypertonie | > 140 | oder | > 90 |

[Herz 1/4](https://www.youtube.com/watch?v=x2_Elv_ceYA), [Herz 2/4](https://www.youtube.com/watch?v=qmy98HNpjOM), [Herz 3/4](https://www.youtube.com/watch?v=n_tKRmrj-tE), [Herz 4/4](https://www.youtube.com/watch?v=tID2fPR2pA8)  
{: .youtube}
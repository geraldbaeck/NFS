title:          Nervensystem
date:           2019/01/06
version:        0.0.7
authors:        gerald@baeck.at
ankikat:        Anatomie
                Notfallmedizin
md5:            993953f24b47837a16b800333ca5851e
date_modified:  2019/06/07

## Aufgaben

+ Sinneswahrnehmung
+ Information ins Hirn leiten (afferent)
+ Adäquate Antwort weiterleiten (efferent)
+ Verarbeitung und Speicherung im Hirn

## Funktionsweise

+ Nervenzellen (Neuronen) leiten elektrische Impulse weiter
+ Spannungspotenziale (Aktionspotenzial, Na+) werden über Natrium, Kalium und Calcium geregelt (ca 70mV) = chemische Übertragung
+ Calciumkanäle = Neurotransmitter
+ Synapse: Stelle einer neuronalen Verknüpfung, über die eine Nervenzelle in Kontakt zu einer anderen Zelle steht

## Zentrales Nervensystem

+ = Gehirn und Rückenmark
+ von Liquor umgeben
+ von Gehirn- bzw. Rückenhäuten umschlossen
+ mechanischer Schutz durch Schädel und Wirbelsäule/-kanal
+ Dämpfung durch Liquor

+ Kommandozentrale
+ interpretiert ankommende Sinnesempfindungen
+ gibt Impulse an die Muskeln ab
+ Weiterleitung der Impulse über Nervenbahnen
+ Gehirn führt komplexe Funktionen aus wie zb. Lernen, Gedächtnis, Gefühle und abstraktes Denken
+ Rückenmark = Verbindung zwischen dem Gehirn und dem peripheren Nervensystem
    + einfache Reflexe bereits auf Rückenmarksebene verschaltet, ohne das Gehirn mit einzubeziehen

### Gehirn

+ Großhirn (Telencephalon)
    + zwei Gehirnhälften, verbunden mit "Balken"
    + Sitz des Bewusstseins, Leistung, Intelligenz
    + Schaltzentrale des Nervensystems
    + Aufnahme und Verarbeitung von Sinneseindrücken
    + steuert Bewegung
    + Aufbau:
        + Großhirnrinde (Cortex)
        + Lappen:
            + Frontallappen
            + Temporallappen
            + Scheitellappen
            + Hinterhauptlappen
+ Zwischen- und Kleinhirn
+ Kleinhirn (Cerebellum)
    + Koordinationszentrum für Bewegungsabläufe
    + verantwortlich für aufrechte Haltung
    + räumliche Orientierung
+ Hirnstamm
    + Hauptzentrum des vegetativen Nervensystems
    + steuert alle Lebensvorgänge (Atem, Temperatur, Kreislauf)
    + Aufbau:
        + Verlängertes Rückenmark
        + Brücke (Pons)
        + Mittelhirn (reflexe, Augenbewegung)
+ Hirnhäute
    + Dura Mater
    + Arachnoidea
    + (Liquor)
    + Pia Mater
+ Hirngefäße
    + A. carotis
    + A. vertebralis
    + A. cerebri media
+ Hirnnerven
    + Sinnesorgane
    + Gesicht
    + (Schluckakt)
    + zehnter Nerv = Nervus Vagus
        + versorgt Körper mit parasympathischen Fasern

### Zwischenhirn (Diencephalon)

+ Schaltstelle zwischen Großhirn und Bereichen des zentralen Nervensystems
+ zwischen dem Großhirn und vegetativen Nervensystem.

### Rückenmark

+ ebenfalls von den Hirnhäuten umgeben
+ liegt im Wirbelkanal
+ reicht ca. bis zum 1. Lendenwirbel
+ Nervenwurzeln in jedem Segment
+ Besteht aus Nervenzellen und Nervenfasern
+ Aufgabe, Nervenimpulse vom Körper zum Gehirn sowie Impulse vom Gehirn zum Körper weiterzuleiten
+ verantwortlich für eine große Zahl lebenswichtiger Reflexe

[Das Gehirn - Zentrales Nervensystem (ZNS)](https://www.youtube.com/watch?v=KU62jMxA5oc){: .youtube}

## Peripheres Nervensystem

Besteht aus Empfindungs- und Bewegungsnerven. Beide liegen meistens in einem gemeinsamen Nervenstrang.

+ **Empfindungsnerven** (afferent): nehmen Empfindungen auf: Wärme, Kälte, Schmerz, usw.: und leiten die Impulse über Rückenmark zu Gehirn weiter.
+ **Bewegungsnerven** (efferent): erhalten Impulse vom Gehirn über RM und leiten zu den entsprechenden Muskeln weiter.
+ 12 Hirnnerven
+ 31 Spinalnerven

[Peripheres Nervensystem einfach erklärt](https://www.youtube.com/watch?v=18JPgHnUvLQ){: .youtube}

## Somatisches Nervensystem

+ Bewusste Wahrnehmung
+ Bewusste Steuerung

## Vegetatives Nervensystem (viszerales-, autonomes-)

+ Hauptsitz im Hirnstamm
+ regelt alle Lebensfunktionen unabhängig vom Willen
+ Reguliert Atmung, Kreislauf, Blutdruck, Verdauung usw.
+ regelt innere Homöostase
+ Besitzt zwei Arten von Nervenfasern, die zu allen Organen des Körpers gelangen und dort Wirkung entfalten (zB: Verengung/Erweiterung der Blutgefäße).

### Sympathikus (fight or flight)

+ dominiert wenn Mensch in einer psychischen oder physischen Ausnahmesituation ist (Stress)
+ Muskeln arbeiten verstärkt, brauchen mehr Energie und Sauerstoff
+ Atemfrequenz erhöht, Bronchien erweitern sich für höhere Luftzufuhr
+ Herzkranzgefäße erweitert, Herzfrequenz erhöht, Puls steigt

### Parasympathikus (Ruhenerv)

Dominiert wenn Mensch ruhig und entspannt ist (Schlaf). Herz- und Atemfrequenz langsamer, Bronchien und Herzkranzgefäße verengen sich. Verdauungsfunktionen aktiv

[Somatisches und vegetatives Nervensystem](https://www.youtube.com/watch?v=MuMNXg6_i9M){: .youtube}
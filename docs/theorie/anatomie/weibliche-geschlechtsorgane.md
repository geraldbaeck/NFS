title:          Weibliche Geschlechtsorgane
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Anatomie
                Notfallmedizin
md5:            30d3cad7e902eb8050f034c71ea3ab67
date_modified:  2019/04/20

## Aufbau

+ Innere Geschlechtsorgane
    + Eierstöcke
    + Eileiter
    + Gebärmutter
    + Gebärmutterhals
    + Scheide
+ Äußere Geschlechtsorgane
    + Venushügel
    + Große- und kleine Schamlippen
    + Kitzler
    + [Scheidenvorhof](https://de.wikipedia.org/wiki/Scheidenvorhof) ist Teil der Vulva, der zwischen kleinen Schamlippen liegt.

Gebärmutter ist ein ca. 8cm großer Hohlmuskel zwischen Blase und Mastdarm ragt mit Gebärmutterhals in Scheide. Von Eierstöcken (Eizellen liegen in Follikel) führen die Eileiter (hier findet Befruchtung statt) beidseits zur Gebärmutter. In Eierstöcken liegen die Eizellen in kleinen flüssigkeitsgefüllten Bläschen (Follikel).

Während Menstruationszyklus reift Follikel heran, drängt zur Oberfläche, platzt und reife Eizelle wird ausgestoßen (=Eisprung). Gleichzeitig wird Gebärmutterschleimhaut für ev. Einnistung aufgebaut. Findet keine Befruchtung statt wird Schleimhaut abgelöst und unter Blutung über Scheide ausgeschieden.

![Weibliche Geschlechtsorgane](https://upload.wikimedia.org/wikipedia/commons/e/e3/Female_anatomy_with_g-spot-de.svg)
Weibliche Geschlechtsorgane[^1]
{: .imagewithcaption}

[^1]: Quelle: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Female_anatomy_with_g-spot-de.svg), [CC-BY-SA-3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.en)
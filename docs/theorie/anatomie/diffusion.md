title:          Diffusion
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Anatomie
                Notfallmedizin
md5:            e96f809e4cefd8eb718472bfb529bc06
date_modified:  2019/06/04
date:           2019/04/20

## Definition

+ =physikalischer Prozess, der zum Ausgleich von Konzentrationunterschieden führt
+ wird durch die zufällige Eigenbewegung der Stoffteilchen (Brown'sche Molekularbewegung) verursacht
+ => gleichmäßigen Verteilung und Durchmischung von Stoffen
+ => Erhöhung der Entropie nach sich zieht

+ =Vermischen von Stoffen, durch eine durchlässige Membran (oder ohne)
+ bis die Moleküle beider Stoffe gleichmäßig im Raum (Lösung) verteilt sind.

Faktoren:

+ große Austauschfläche
+ kurzer Diffusionsweg
+ lange Kontaktzeit
+ großer Konzentrationsunterschied

Beispiel: Gasaustausch zw. Alveolen und Kapillaren (02-C02)
title:          Kreislaufsystem
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            06112324d017d2a882e66134b6077078
date_modified:  2019/01/07

## Definition

= bedarfsgerechter Transport von Sauerstoff und Energie im Organismus

Jede Störung des Kreislaufs zieht Störung der Durchblutung und somit Sauerstoffmangel nach sich. Führt zur Störung des Zellstoffwechsels, zur Störung des Säure-Basen-Haushaltes und Störung des Wärmehaushaltes. Im Gegenzug beeinflussen Störungen des Wärmehaushaltes, Säure-Basen-Haushaltes und Wasser-Elektrolyt-Haushaltes den Kreislauf.

## Komponenten

+ Herz kommt als Zentralorgan Pumpfunktion
+ Blutgefäße und Blut
+ zentrale Kreislaufsteuerung und Blutdruckregulation

## Übersicht

Der Blutkreislauf besteht aus **Lungenkreislauf** (Funktion: Gasaustausch) und **Körperkreislauf** (Funktion: Gasaustausch und Ernährung etc.) und wird durch Pumpfunktion des Herzens aufrechterhalten.

=> sauerstoffreiches Blut  
=> Linker Vorhof  
=> linke Kammer  
=> Aorta  
=> Arterien  
=> Arteriolen  
=> Kapillaren  
=> Gasaustausch  
=> Venolen  
=> Venen  
=> Vena Cava  
=> Rechter Vorhof  
=> Rechte Kammer  
=> Lungenarterie  
=> Gasaustausch  
=> Lungenvene  
=> Linker Vorhof ===>
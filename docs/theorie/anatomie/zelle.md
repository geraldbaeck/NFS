title:          Zelle
date:           2019/03/07
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            83b6ad5361f632b4ceb168f0196f67d6
date_modified:  2019/03/15

## Definition

= kleineste Bau- und Funktionseinheit des Lebendigen

## Grundfunktionen

+ Reizbeantwortung
+ Eigenbewegung
+ Stoffwechsel
+ Teilung

## Aufbau

+ Zellkern (Nukleus)
    + Nukleolus
+ Zellleib (Zytoplasma) (mit Organellen)
    + Endoplasmatisches Retikulum: Kanälchensystem rund um den Zellkern, dient als Transport- und Speichersystem
    + Ribosomen: dienen der Proteinsynthese
    + Golgi-Apparat: TODO
    + Lysosomen: TODO
    + Mitochondrien: TODO
    + Zentriolen: TODO
+ Zellmembran

## Funktionen

| --- | --- |
| mechanische Kraft | Muskelzelle |
| Leitung von (elektr.) Impulsen | Nervenzelle |
| chemische Prozesse (Stoffwechsel) | Leberzelle |
| Abdeckung von Oberflächen | Epithelzelle |
| Sauerstoffstransport | rotes Blutkörperchen (Erythrozyt) |
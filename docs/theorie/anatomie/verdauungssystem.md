title:          Verdauungstrakt
date:           2019/01/06
version:        0.0.7
authors:        gerald@baeck.at
ankikat:        Anatomie
                Notfallmedizin
                HeavyRotation
md5:            ebe9c219d49ac1050b2a8c093202bbb5
date_modified:  2019/06/16

Verdauungsorgane dienen zur Aufnahme, Weitertransport und Verarbeitung von Nahrung.

## Weg der Nahrung

+ Nahrungsaufnahme  
+ **Mundhöhle**
    + Zähne zerkleinern
    + Zunge
        + tasten & schmecken
        + sprechen
        + schlucken
        + kauen
    + Speichelvermengung aus den Drüsen (Ohr-, Unterkiefer- & Unterzungenspeicheldrüse)
        + rutschig machen
        + antimikrobiell durch Lysozyme
        + Kohlehydrate spalten (durch Amylase)
+ Pharynx (Rachen)
    + Weiche verschließt Nase und Luftröhre
+ **Speiseröhre**  (Ösophagus)
    + Muskelschlauch, oben quer gestreift, unten längs
    + Oberer Ösophagus sphinkter verhindert, dass Luft in den Magen gelangt
    + Unterer Ösophagus sphinkter, verhindert Magensaftaustritt
    + peristaltisch
+ **Magen**
    + Aufspaltung durch Verdauungsfermente im salzsäurehaltigen Magensaft  
+ Magenausgang (**Pförtner**)  
+ **Zwölffingerdarm**
    + Fermente aus Bauchspeicheldrüse & Gallensaft beigemengt
    + = Nahrung wird in kleinste Bausteine zerlegt  
+ **Dünndarm**
    + Nährstoffaufnahme (Resorbtion) durch Dünndarmzotten
    + Nährstoffe gehen über das venöse Blut des Darms direkt in die Pfortader => Leber
+ **Dickdarm**
    + keine Verdauung mehr
    + durch Wasserentzug Eindickung des Speisebreis
    + Blinddarm & Wurmfortsatz sind hier
+ Ausscheidung über **Mastdarm**

[Magendarm Trakt Teil 1](https://www.youtube.com/watch?v=JKEjo2M6wiA), [Magendarm Trakt Teil 2](https://www.youtube.com/watch?v=IOEqnDZ-vuo){: .youtube}

## Leber & Galle

### Leber

+ rechter Oberbauch
+ geschützt durch Rippenbogen
+ größte Drüse im Körper.

### Aufgaben  

+ Pfortader bringt Nährstoffe aus Dünndarm zur Leber (chemische Umwandlung)
    + Speichern von Zucker, Vitaminen und Fetten
    + Entgiftung von fettlöslichen Produkten (zb Medikamente)
    + Bildung von Proteinen/Albumin & Gerinnungsfaktoren
+ Produktion von Galle
    + fließt tlw in den Darm oder
    + Speicherung in Gallenblase

[Leber erklärt](https://www.youtube.com/watch?v=PBz9S6SOpOY){: .youtube}

## Bauchspeicheldrüse

+ liegt hinter dem Magen
+ bildet Verdauungsfermente die in den Zwölffingerdarm gelangen
+ bildet ein Eiweiß-auflösendes Enzym
+ produziert lebenswichtige Hormone z.B.: Insulin und Glukagon die direkt ins Blut abgegeben werden.

[Bauchspeicheldrüse/Pankreas](https://www.youtube.com/watch?v=L-lOXDrD030){: .youtube}

## Peritoneum (Bauchfell)

+ Peritoneum parietale
    + kleidet als seröse Haut den Bauchraum aus
+ Peritoneum viscerale
    + umgibt die meisten inneren Organe unterhalb des Zwerchfells bis zum Eingang des kleinen Beckens
    + sensibel invertiert (=schmerzempfindlich)
+ Peritonealhöhle = Raum zwischen parietale und viscerale
+ einschichtiges, flaches Epithel
+ beherbergt die Blutgefäße, die Lymphgefäße und die Nerven der Bauchorgane

[Peritoneum Sagittalschnitt](https://tv.doccheck.com/de/movie/74286/peritoneum-12-sagittalschnitt)

[Peritoneum Transversalschnitt](https://tv.doccheck.com/de/movie/74312/peritoneum-23-transversalschnitt)
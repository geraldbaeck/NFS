title:          Blutgefäße
date:           2019/01/06
version:        0.0.5
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            c245884d93567bd0dbd030c4ad7de14c
date_modified:  2019/03/15

## Aufgaben

+ Leiten/führen des Blutes
+ Zuführen von Nährstoffen und Sauerstoff
+ Abführen von Abfallstoffen und CO&#8322;
+ Regulierung des Blutdrucks
+ Regelung der Körpertemperatur

## Aufbau

+ Tuncia externa/adventitia
+ Tunica media (Für Verengung und Erweiterung verantwortlich => Druck und Temperatur)
+ Tunica interna/intima

## Arterien

+ dickwandig
+ elastisch
+ pumpen vom Herzen weg (wenn das Herz zusammengezogen ist)

## Kapillaren (Haargefäße)

+ Treffpunkt von Arteriolen und Venolen
+ langsamer Blutfluss
+ enger Kontakt zu Gewebe (Austausch)
+ sind die feinsten Aufzweigungen der Blutgefäße
+ Gasaustausch
+ ein Blutkörperchen passt durch
+ Besteht aus einer einzigen durchlässigen Zellschicht (Endothelschicht)

## Venen

+ dünnwandig
+ größerer Durchmesser als Arterie
+ fließt zum Herzen zurück
+ haben zum Teil Klappen, um die Flussrichtung zu bestimmen

![Herzkreislauf erklärt 2](https://www.youtube.com/watch?v=HYt969HUxeo)
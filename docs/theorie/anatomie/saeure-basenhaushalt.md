title:          Säure- Basenhaushalt
date:           2019/01/06
version:        0.0.7
authors:        gerald@baeck.at
ankikat:        Anatomie
                Notfallmedizin
md5:            d51cb9de67a6c41718aee3b3c4ce600b
date_modified:  2019/05/03

+ Bezeichnung für diverse physiologische Regelmechanismen nach dem Prinzip der Homöostase (Gleichgewichtszustandes)
+ regelt das Gleichgewicht aller Lebensvorgänge
+ chemischen Prozesse im Körper sind nur in annähernd neutralem Bereich möglich = pH 7,4 (+-0.05)
+ Säureüberschuss durch Ernährung von täglich ca. 40-80 mmol

## Regelungsmechanismen

+ Pufferung (=Abfang der Wasserstoffionen bei der Entstehung)
    + im Blut
    + im Gewebe (Bikarbonate)
+ CO&#8322; Ausscheidung über die Lunge (pulmonale Regulation)
+ Wasserstoffionen Ausscheidung über die Niere (renale Regulation)

## Störungen

| Störung | Ursache | Maßnahme |
| --- | --- | --- |
| respiratorische Azidose | Dyspnoe, Asphyxie | O&#8322;, Beatmen |
| respiratorische Alkalose | Hyperventilation | Totraum vergrößern |
| metabolische Azidose | Schock, Stoffwechselstörung, DM | O&#8322;, Grundstörung beheben |
| metabolische Alkalose | starkes Erbrechen | - |

[Die respiratorische Azidose](https://www.youtube.com/watch?v=-fWcqGobYeo){: .youtube}
title:          Männliche Geschlechtsorgane
date:           2019/01/06
version:        0.0.5
authors:        gerald@baeck.at
ankikat:        Anatomie
                Notfallmedizin
md5:            39c66120db7d34a479ae36015bba38e4
date_modified:  2019/04/21

## Aufbau

+ Innere Geschlechtsorgane
    + Hoden (Keimdrüsen, bildet Spermien und Testosteron)
    + Nebenhoden (Speicherung u. Reifung der Spermien => Mobilität)
    + Samenleiter
    + Samenbläschen (prod. flüssigen Anteil des Spermas)
    + Prostata (Produktion von Sekret zur Vermischung mit Sperma bei Ejakulation)
    + Bulbourethraldrüse (Präejakulat)
+ Äußere Geschlechtsorgane
    + Hodensack
    + Glied (Penis) mit Harnröhre = Harn- und Geschlechtsorgan

![Männliche Geschlechtsorgane](https://upload.wikimedia.org/wikipedia/commons/9/9d/Male_anatomy_de.SVG)
Männliche Geschlechtsorgane[^1]
{: .imagewithcaption}

[^1]: Quelle: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Male_anatomy_de.SVG?uselang=de), [CC-BY-SA-3.0](http://creativecommons.org/licenses/by-sa/3.0)
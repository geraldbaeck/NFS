title:          Organe
date:           2019/03/07
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            60584b3f270e8b6723d3abd557628023
date_modified:  2019/03/15

## Definition

eine umschriebene (mit dem freien Auge sichtbare) Funktionseinheit des Körpers

+ Gehirn
+ Herz
+ Lunge
+ Leber
+ Niere
+ Dünn- & Dickdarm (Gastrointestinaltrakt)
+ uvm.

## Aufbau

+ spezielles Gewebe des Organs
+ organbegrenzende Kapsel oder Haut = Bindegewebe
+ zu und abführende Gefäße ("Stiel")
+ Epithelgewebe and den inneren und äußeren Flächen
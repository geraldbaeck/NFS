title:          Lymphsystem, Immunsystem
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            1517c25e8e4152fc401bee7dae4976dc
date_modified:  2019/01/07

## Aufgaben  
+ ergänzt das Blutsystem
+ transportiert
    + Körperflüssigkeiten
    + Blutzellen
    + Nährstoffe
+ Immunabwehr
+ Regulation des Wasserhaushalts im Gewebe

## Bestandteile  
+ Lymphgefäßen und
+ primären Organen (erzeugen Lymphozyten)
    + Knochenmark
    + Thymus
+ sekundäre Organe (aktivieren Lymphozyten)
    + Mandeln (Tonsillen)
    + Milz
    + Blinddarm  
    + Peyersche Plaques
    + Lymphknoten

## "Kreislauf"  
+ Flüssigkeit tritt aus den Kapillaren aus
+ sog Gewebswasser = interzelluläre Flüssigkeit
+ Gewebswasser + Ausscheidungsprodukte der Zellen = Lymphe
+ Lymphe fließt => Lymphkapillaren => Lymphbahnen => obere Hohlvene

+ insgesamt ca 2L täglich
+ transport eher passiv
+ ist eigentlich kein Kreislauf sondern ein Entwässerungssystem fürs Gewebe

[Das Lymphsystem - Organe des Menschen](https://www.youtube.com/watch?v=ClUOcvgAOnY){: .youtube}
title:          ALARA Prinzip
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        nice2know
md5:            ec7578719622a5bfe644642a6208b4ca
date_modified:  2019/04/01
date:           2019/04/01

Prinzip des Strahlenschutzes. Das ALARA-Prinzip fordert beim Umgang mit ionisierenden Strahlen eine so niedrige Strahlenbelastung von Menschen, Tieren und Material (auch unterhalb von Grenzwerten), wie sie unter Einbeziehung praktischer Vernunft und Abwägung von Vor- und Nachteilen machbar erscheint.

+ **A**s
+ **L**ow
+ **A**s
+ **R**easonable
+ **A**chieveable
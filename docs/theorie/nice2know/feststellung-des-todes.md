title:          Feststellung des Todes
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
md5:            9203973fd34529ee5b87d74beff2e00e
ankikat:        nice2know
date_modified:  2019/01/07

## Definition

+ **klinischer Tod**: Stillstand von Atmung & Kreislauf
+ **biologischer Tod**: Absterben aller Zellen


## Sichere Todezeichen

+ Mit dem Leben nicht vereinbare Verletzungen (Kopfabtrennungen, totale Deformation)
+ Totenflecken
+ Totenstarre: 1-2h Stunden, löst sich nach 2-3d
+ Verwesungserscheinungen: Leichengeruch, Fäulnis, aufgetriebener Bauch, Ausfluss von übelriechender Flüssigkeit aus dem Körper


## Exekutive verständigen bei

+ an öffentlichen Plätzen
+ Suizid
+ Fremdverschulden
+ Minderjährigen
+ Öffnungen der Wohnung
+ Unfall
+ Identität unbekannt
+ Ausländen Staatsbürgern
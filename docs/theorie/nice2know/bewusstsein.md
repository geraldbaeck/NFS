title:          Bewusstsein
date:           2019/01/06
version:        0.0.8
authors:        gerald@baeck.at
md5:            cf4da54ef69baab21fa1300e95391dfa
ankikat:        nice2know
                Notfallmedizin
                HeavyRotation
date_modified:  2019/06/07

## Definition

+ Aufnahme von Sinneseindrücken
+ Gedächtnis und Merkfähigkeit
+ Ausführung gezielter Bewegungen
+ intakte Schutzreflexe

Ungestörtes Bewusstsein ermöglicht also Sehen, Hören, Fühlen, Schmecken, Riechen, Denken, Merken, geordnetes Bewegen, Orientieren und Reagieren.

## Ursachen

Eine Bewusstseinsminderung ist immer ein Symptom einer körperlichen Gesundheitsstörung. Dabei kommen in Betracht:

+ Intoxikation
+ Trauma
    + SHT
    + Stromunfall
+ Krampfanfälle
+ intracerebrale Raumforderungen
    + Blutung
    + Hirnödem
    + Hydrocephalus
    + Tumore
+ Ischämien
+ Metabolische Störungen
    + Leberversagen
    + Nierenversagen
    + Elektrolytentgleisung
    + Meningitis/Enzephalitis
    + Hypoxie
+ Endokrine Ursachen
    + Hypo-/Hyperglykämie
    + psychiatrische Erkrankungen 

## Kategorisierung

+ Benommenheit
    + Denken und Handeln deutlich bis hin zur Apathie verlangsamt
    + Orientierungsfähigkeit herabgesetzt
    + Geringe spontane sprachliche Äußerungen, langsames Denken und reduzierte Auffassungsgabe
    + leicht weckbar
+ Somnolenz
    + beständige Schläfrigkeit oder Schlafneigung
    + kann durch einfache Weckreize jederzeit unterbrochen werden
    + Keine spontanen sprachlichen Äußerungen; wenn doch, dann unverständlich (Murmeln)
    + Reflexe sind erhalten.
    + Herabgesetzte Konzentration und Aufmerksamkeit
+ Sopor
    + Schlafgleicher Zustand
    + nur noch mit Mühe, etwa Schmerzreize, aufzuwecken
    + Nicht mehr orientiert
    + keine sprachlichen Äußerungen
    + Reflexe erhalten
+ Koma
    + höchste Grad der Bewusstseinsverminderung
    + kann nicht geweckt werden
    + Keine Abwehrbewegungen
    + neurologisch können dabei anhand zunehmender Reflexausfälle weitere Grade bis hin zum tiefsten Koma unterschieden werden, in dem zentrale Lebensreflexe erloschen sind und größte Lebensgefahr durch Aussetzen der Atmung besteht
    + Sonderfälle: Wachkoma, künstliches Koma

## Kollaps

Kurzes Kreislaufversagen durch eine Minderdurchblutung (O&#8322;-Mangel) im Hirn.

## Synkope

Kurze Bewusstlosigkeit auf Grund eines zerebralen Geschehens (Insult, Epi) oder auf Grund einer Kreislaufstörung.
title:          Vokabel
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
date_modified:  2019/01/07

<script>
    var waitForJQuery = setInterval(function () {
        if (typeof $ != 'undefined') {

            $(document).ready(function(){
                $.when(
                    $.getScript("https://cdnjs.cloudflare.com/ajax/libs/PapaParse/4.6.2/papaparse.min.js"),
                    $.getScript("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"),
                    $.Deferred(function( deferred ){
                        $( deferred.resolve );
                    })
                ).done(function(){
                    $.getScript("https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js", function() {

                        Papa.parse("../vokabel.csv", {
                            download: true,
                            complete: function(results) {
                                var table = $('<table class="table table-striped table-hover" id="vokabel_table"></table>');
                                $("#table_div").append(table);
                                $('#vokabel_table').DataTable({
                                    data: results["data"],
                                    columns: [
                                        {title: ""},
                                        {title: ""}
                                    ],
                                    paging: false,
                                    ordering: true,
                                    "language": {
                                        "search": "filtern:"
                                    }
                                });
                            }
                        });
                    });

                });
            });

            clearInterval(waitForJQuery);
        }
    }, 10);
</script>

<div id="table_div"></div>
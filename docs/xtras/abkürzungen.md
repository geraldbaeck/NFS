title:          Abkürzungen
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
md5:            e5483f8715695fc758fd71bee3c2c55c
date_modified:  2019/01/07


|   |   |
|:---|:---|
| SARS | Schweres Akutes Respiratorisches Syndrom |
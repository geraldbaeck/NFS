title:          Pulsoximetrie
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        praxis
                Notfallmedizin
md5:            a4cfb800af182fe35b1ff52be7838e57
date_modified:  2019/04/28
date:           2019/04/20

## Definition

+ = Verfahren zur nichtinvasiven Ermittlung der arteriellen Sauerstoffsättigung
+ durch Messung der Lichtabsorption bei Durchleuchtung der Haut (perkutan)

+ Messung der Lichtabsorption durch Hämoglobin (Blutfarbstoff, Hb)
    + Sauerstoffsättigung in Prozent (O&#8322;-Hb)
    + Pulswelle
    + Puls-/Herzfrequenz

## Fehlermöglichkeiten

+ technische Artefakte
    + Vibrationen während der Fahrt
    + starkes Umgebungslicht
    + Mangelnde Fixierung
+ patientenbezogene Artefakte
    + Hypoperfusion: zu kalte oder schlecht durchblutende Gefäße
    + pathologisches Hämoglobin
    + Dunkler Nagellack
    + Ikterus (Gelbsucht)
    + Hautpigmentierung
    + Anämie
+ Fehlinterpretationen
    + CO-Vergiftung
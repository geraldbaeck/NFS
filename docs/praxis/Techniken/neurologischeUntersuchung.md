title:          Orientierende Neurologische Untersuchungen
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        praxis
                Notfallmedizin
md5:            0cb822059ea30bf4fadc662b8ea6d523
date_modified:  2019/04/20
date:           2019/04/20

+ Orientierung
    + zeitlich
    + örtlich
    + persönlich
    + situativ
+ Aufnahmefähigkeit
+ Aufmerksamkeit
+ Augenöffnung
+ Pupillenreaktion auf Licht
+ verbale Kommunikation / Sprache
+ Motorische Bewegungen auf Schmerzreiz
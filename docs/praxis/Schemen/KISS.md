title:          KISS Schema
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        praxis
md5:            f663f99f4606d3dc60e215b725190ee2
date_modified:  2019/04/01
date:           2019/04/01

= kurzer Behandlungsalgorithmus für Beckenverletzungen

+ **K**inematik, die eine Beckenverletzung möglich oder wahrscheinlich macht
+ **I**nspektion, die eine Beckenverletzung vermuten lässt (sichtbare Verletzung, Blutung, Fehlstellungen)
+ **S**chmerzen im Bereich des Beckens oder der unteren Extremitäten
+ **S**tabilisierung des Beckens im Sinne einer Beckenschlinge indiziert, wenn eines positiv ist
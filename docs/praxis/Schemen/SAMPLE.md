title:          SAMPLE
desc:           Erhebung der Daten durch Befragung des Patienten oder Fremdanamnese.
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
md5:            4b35552c9ec2d7a07c7d1df7140a3d44
ankikat:        praxis
date_modified:  2019/01/07

## **S** Symptome/Schmerzen

+ Welche Beschwerden liegen vor?
+ Wie fühlen sich die Schmerzen an? (dumpf/stechend)
+ Strahlen die Schmerzen aus?
+ Seit wann bestehen die Symptome?


## **A** Allergien


## **M** Medikamente

+ Welche Medikamante werden eingenommen?
+ Wann wurden die Medikamente zuletzt eingenommen?


## **P** Patientengeschichte

+ Vorerkrankungen? (Diabetes, Bluthochdruck, Herzerkrankungen, Schrittmacher, Krebs....)
+ Operationen
+ auch mögliche Schwangerschaft


## **L** Letzter Zeitpunkt von

+ Nahrungsaufnahme
+ Stuhlgang
+ Menstruation
+ Artzbesuch
+ Flüssigkeitsaufnahme
+ Krankenhausaufenthalt


## **E** Ereignis

+ Was ging den Beschwerden voraus?
+ Was hat der Patient gemacht, als die Beschwerden begannen?
+ Was hat die Symptome ausgelöst?


## **R** Risikofaktoren

+ Gewicht
+ Alter
+ Geschlecht
+ Nikotin- oder Drogenabusus
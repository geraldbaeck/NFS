title:          Cushing Triade
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        praxis
md5:            6542c4425df3c438b7fa986da16b8a35
date_modified:  2019/05/14
date:           2019/04/22

= Blutdrucksteigerung mit gelegentlichem Herzfrequenzabfall bei Zunahme des Hirndrucks

## Symptome

Dienen zur Diagnostik eines Raumforderungsproblems in Gehirn:

+ Hypertension
+ Bradykardie
+ Unregelmäßige Atmung
    + Cheyne Stokes Atmung
    + Biot Atmung
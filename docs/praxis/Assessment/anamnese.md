title:          Anamnese
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        praxis
                Notfallmedizin
md5:            8c29fa2df57bb63fa073b47e7a82c647
date_modified:  2019/06/07
date:           2019/04/20

## Definition

= die professionelle Erfragung von potenziell medizinisch relevanten Informationen durch Fachpersonal

+ Ziel: Erfassung der Krankengeschichte im Rahmen einer aktuellen Erkrankung
    + Beschwerden
    + psychische -
    + soziale Aspekte

## Arten der Anamnese

nach Quelle:

+ Eigenanamnese
+ Fremdanamnese

inhaltlich:

+ Psychosoziale Anamnese
+ Familienanamnese
+ Sozialanamnese
+ Sexualanamnese
+ Suchtanamnese
+ Medikamentenanamnese
+ körperliche Anamnese
+ vegetative Anamnese
+ Ernährungsanamnese

## Die wichtigsten Fragen/Schemen

+ ABCDE
+ SAMPLER
+ OPQRST
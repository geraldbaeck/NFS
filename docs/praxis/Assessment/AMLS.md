title:          AMLS
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        praxis
                Notfallmedizin
template:       sop.html
md5:            c90dbb7737f15fd98f014707ca39347e
date_modified:  2019/04/01
date:           2019/04/01

## 3S

### Scene, Safety, Situation

+ Scene: Einsatzmeldung; Was, Wo, Wie, Wetter?; Equipment
+ Safety: Einsatzstelle, PSA, Helme, Warnwesten, Schutzbrille
+ Situation: Wie viele Verletzte? Zusätzliche Rettungsmittel?

## GI

### General Impression

+ Bewusstsein
    + AVPU
    + Patientenverhalten (zittern, agitiert, etc.)
+ Hautbild
    + Farbe
    + Schweiß?
+ Atmung
    + Atemmuster
    + Tachypnoe / Bradypnoe
+ Umfeld
+ Maßnahmen: ***Lagerung, Sauerstoff, Defipads kleben***

## A

### Airway

+ Inspektion (Aufforderung Zunge herauszustrecken, Mund manuell öffnen)
+ partielle Verlegung durch Zunge => ***Esmarch => Güdel/Wendel***
+ Verlegung durch Fremdkörper => ***Absaugen, Ausräumen***

## B

### Breathing

+ Trachea mittelständig?
+ Halsvenen gestaut?
+ Atemfrequenz auszählen => <10 >30 => ***assistierte Beatmung***
+ Gebrauch der Atemhilfsmuskulatur?
+ hebt beidseitig?
+ Auskultieren immer im Seitenvergleich
+ Maßnahme: ***Sauerstoff, Lagerung***

## C

### Circulation

+ Pulsmessung beidseitig, ggf auch zentral
    + Qualität
    + Stärke
    + Rhythmus
    + Frequenz
+ Haut
    + Kolorit
    + Schweißig?
    + Temperatur
+ Rekap
    + peripher ggf auch zentral
+ Maßnahme: ***Defipads kleben, Volumengabe, Lagerung (CAVE: MCI)***

## D

### Disability

+ Pupillenbefund
+ Halbseitensymptomatik
    + ggf FAST+
    + ggf APSS bestimmen
+ GCS bestimmen

## E

### Etablierung der Hauptbeschwerde / Leitsymptom

+ Körperübersicht, suche nach
    + Ausschlägen
    + Zungenbiss
    + Ödeme
    + Decubita
    + Harn- und Stuhlabgang
+ Maßnahme: ***Wärmeerhalt***

## E

### 10 for 10

+ Leitsymptom etablieren
+ Untersuchungsergebnisse zusammenfassen
+ Differentialdiagnosen sammeln (Trichter füllen)
+ **Entscheidung kritisch / nicht kritisch**

## &nbsp;

### Weiterführende Anamnese und Untersuchung

+ S - OPQRST & AMPLER - S
+ Fokussierte Untersuchung
+ Vitalwerte: AF, S<sub>p</sub>O&#8322;, HF, RR, EKG, KKT, BZ

## &nbsp;

### Finally another 10 for 10

+ Spezifische Maßnahmen
+ Neubeurteilung
+ Anpassen der Verdachtsdiagnose
+ Lagerung des Patienten
+ Transportentscheidung
+ **Reevaluierung nach jeder Lageänderung und jeder Maßnahme**
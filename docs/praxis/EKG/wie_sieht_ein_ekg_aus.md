title:          Wie sieht ein EKG aus?
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        ekg
md5:            b66e536e6efaea385a7c6b25dbbc1e40
date_modified:  2019/01/23
date:           2019/01/23

![Sinus Rythmus](https://upload.wikimedia.org/wikipedia/commons/f/ff/SinusRhythm_withoutLabels.svg)
Normaler Sinusrythmus des menschlichen Herzens[^1]
{: .imagewithcaption}

## Formen
EKG beschreibt eine Sinuskurve mit unterschiedlichen Ausschlägen:

### positiver Ausschlag
+ Erregung wandert auf eine Ableitung zu

### negativer Ausschlag
+ Erregung bewegt sich von der Ableitung fort

### biphasischer Ausschlag
+ Ableitung steht senkrecht zur Ausbreitungsrichtung

## Amplitude / Größe
+ Summe aller vorhandenen Spannungen
+ minus Dämpfung durch Gewebe und Thorax

## Wellen  
| Typ | Beschreibung | |
| --- | --- | --- |
| **P**-Welle | Vorhoferregung | |
| **QRS**-Komplex | Kammererregung | |
| **T**-Welle | Erregungsrückbildung | nur Repolarisation, keine Muskelkontraktion |
title:          Das normale EKG
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        ekg
md5:            297ec4fd96a27b3b4f9bbd48a7c3c77a
date_modified:  2019/01/23
date:           2019/01/23

Man erkennt den normalen PQRST Ablauf am besten in Ableitung II.

## P-Welle
+ Vorhoferregung
+ Sinusknoten sitz rechts und erregt zuerst die rechte Kammer und verzögert links über das Bachmann-Bündel
+ dh. erste Teil der P-Welle = rechter Vorhof, zweiter Teil = linker
+ am stärksten ausgeprägt in II und V&#8322;

## QRS-Komplex
+ Kammererregung
+ Q = erster negativer Ausschlag nach P
+ R-Zacke = erster positiver Ausschlag nach P
+ S-Zacke = negativer Ausschlag nach R

### RS-Komplex
+ folgt auf eine P-Welle eine positive Zacke muss es sich um ein R handeln
+ folgt auf das R eine negative Zacke => RS-Komplex

### QS-Komplex
+ folgt auf eine P-Welle nur ein negativer Aussschlag => QS-Komplex

### geknoteter QRS-Komplex
+ lassen sich Auf- und Abwärtsbewegungen nur schwer trennen => geknotet
+ kann man dagegen zwei positive Ausschläge erkennen, wird der zweite als R´ oder r´ bezeichnet
+ der größer der beiden R-Zacken erhält dabei den Großbuchstaben, genauso bei mehrere S-Zacken

## T-Welle
+ spiegelt die Repolarisierung der Kammern wieder, keine Herzmuskelaktivität
+ manchmal findet sich zwischen zwei QRS-Komplexen nur eine Welle, meist T-Welle , mehrere Ableitungen prüfen
+ manche Personen haben nach der T- noch eine kleiner U-Welle

## Zeiten

| EKG | Dauer | Vorschub (25 mm/s aka 5 Kasteln/s) |
| --- | --- | --- |
| P-Welle | bis 0,1 s | 2,5 mm |
| PQ-Zeit | 0,12 - 0,21 s | 3 - 5 mm |
| QRS-Komplex | =< 0,12 s | 3 mm |
| QT-Zeit | 0,3 - 0.45 s | 10mm |
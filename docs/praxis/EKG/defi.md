title:          Defi Funktionen
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        EKG
md5:            a9b0f636afddc79f12d072966f1abee2
date_modified:  2019/06/18
date:           2019/06/18

+ dient der Behandlung von Patienten mit zu langsamen Herzschläge (Bradykardie)
+ stimuliert regelmäßig den Herzmuskel mit Hilfe von elektrischen Impulsen und regt diesen so zur Kontraktion an.

## Funktionen

Schrittmacherstimulationsmodi werden in international einheitliche Buchstaben kodiert.

| 1. Stimulationsort | 2. Registrierungsort | 3. Betriebsart | 4. Frequenzadaptation | 5. Multifokale Stimulation |
| --- | --- | --- | --- | --- |
| A (Atrium) | A (Atrium) | T (getriggert) | R (adaptiv) | A (Atrium) |
| V (Ventrikel) | V (Ventrikel) | I (inhibiert) |  | V (Ventrikel) |
| D (Dual A+V) | D (Dual A+V) | D (Dual T+I) |  | D (Dual A+V) |

Durch den Spike, (Simulationsimpuls) im EKG als Zacke sichtbar.

## Defibrillation

+ ausschließlich im Rahmen der Reanimation
    + bei Kammerflimmern
    + PVT

## Kardioversion

+ zur Wiederherstellung des normalen Sinusrhythmus bei Herzrhythmusstörungen
    + Vorhofflimmern
    + SVT
    + VT
    + Vorhofflattern

+ Kardioversion mit Defi
    +Schock mit geringer Initialdosis (50-100 Joule)
    + EKG getriggert ausgelöst (es wird in der R-Zacke hineingeschockt)
+ Kardioversion mit Medikamenten
    + Adenosin (6ml-12ml-12ml)
    + Renimationsbereitschaft
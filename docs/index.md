title:          Lernunterlagen zur NFS Ausbildung
desc:           Meine persönlichen Unterlagen zur Ausbildung zum Notfallsanitäter bei der Wiener Berufsrettung 03/2019.
date:           2019/01/06
version:        0.0.5
authors:        gerald@baeck.at
md5:            3e529e6bd9397a66cde08696bfca1566
date_modified:  2019/06/18

## Über diese Seite

Meine persönlichen Lernunterlagen zur Vorbereitung auf den NFS Kurs 2019.

Der Aufbau des Skripts ist oft stichwortartig und mit vielen Bulletpoints versehen, weil das mein Hirn anscheinend so besser verarbeiten kann. Deswegen eigenen sich diese Unterlagen nicht zum erstmaligem Kennenlernen der Materie, sondern vielmehr zur Wiederholung und Auffrischung.

Der Kurs ist jetzt beendet, deswegen wird diese Seite nicht mehr gewartet, aber online bleiben. Alle Unterlagen sind lizenzfrei und können (sofern nicht in den Dokumenten direkt vermerkt) frei und ohne Namensnennung verwendet werden.

## Lizenz

[CC0 1.0 Universell (CC0 1.0)](https://creativecommons.org/publicdomain/zero/1.0/deed.de) Public Domain Dedication

## Haftungsausschluss

Es wird keine Haftung für die Richtigkeit des Inhalts übernommen.
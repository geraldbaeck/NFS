# Lernunterlagen zur NFS Ausbildung

Meine persönlichen Unterlagen zur Ausbildung zum NFS bei der Wiener Berufsrettung 03/2019. Diese Unterlagen dienen vor allem mir persönlich als Lernhilfe, können aber gerne weiter verwendet werden. Intention ist es, bereits gehörtes zu Vertiefen, deswegen wird gerne und viel punktiert und im Stakkato formuliert. Die Unterlagen eigenen sich definitiv nicht für Erstlerner.

Eine Webversion gibt es hier:
**[https://nfs.rettungsnerd.at/](https://nfs.rettungsnerd.at/)**

Der Kurs ist jetzt beendet, deswegen wird diese Seite nicht mehr gewartet, aber online bleiben. Alle Unterlagen sind lizenzfrei und können (sofern nicht in den Dokumenten direkt vermerkt) frei und ohne Namensnennung verwendet werden.

## Installieren

```bash
git clone https://gitlab.com/geraldbaeck/NFS.git
cd NFS
pip install pipenv
pipenv run mkdocs serve
```

## Lizenz

[CC0 1.0 Universell (CC0 1.0)](https://creativecommons.org/publicdomain/zero/1.0/deed.de) Public Domain Dedication

## Haftungsausschluss

Der Autor übernimmt keinerlei Gewähr für die Aktualität, Korrektheit, Vollständigkeit oder Qualität der bereitgestellten Informationen. Haftungsansprüche gegen den Autor, welche sich auf Schäden materieller oder ideeller Art beziehen, die durch die Nutzung oder Nichtnutzung der dargebotenen Informationen bzw. durch die Nutzung fehlerhafter und unvollständiger Informationen verursacht wurden sind grundsätzlich ausgeschlossen, sofern seitens des Autors kein nachweislich vorsätzliches oder grob fahrlässiges Verschulden vorliegt. Alle Angebote sind freibleibend und unverbindlich. Der Autor behält es sich ausdrücklich vor, Teile der Seiten oder das gesamte Angebot ohne gesonderte Ankündigung zu verändern, zu ergänzen, zu löschen oder die Veröffentlichung zeitweise oder endgültig einzustellen. 
